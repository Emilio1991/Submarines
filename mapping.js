////////////////MAPPING////////////////
module.exports.mapping = mapping;
module.exports.findNeighbors = findNeighbors;
module.exports.findNeighbors2 = findNeighbors2;

//return array of hex's neigbors
function findNeighbors(row, column, gameSettings) {
  var UL, U, UR, DL, D, DR;

  if (row == 0 && column == 0) {
    UL = null;
    U = null;
    UR = null;
    DL = null;
    D = [1, 0];
    DR = [0, 1];
  } else if (row == 0 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 == 0) {
    UL = [0, gameSettings.Hw - 2];
    U = null;
    UR = null;
    DL = [1, gameSettings.Hw - 2];
    D = [1, gameSettings.Hw - 1];
    DR = null;
  } else if (row == 0 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 != 0) {
    UL = null;
    U = null;
    UR = null;
    DL = [0, gameSettings.Hw - 2];
    D = [1, gameSettings.Hw - 1];
    DR = null;
  } else if (row == 0 && column % 2 != 0) {
    UL = null;
    U = null;
    UR = null;
    DL = [row, column - 1];
    D = [row + 1, column];
    DR = [row, column + 1];
  } else if (row == 0) {
    UL = [row, column - 1];
    U = null;
    UR = [row, column + 1];
    DL = [row + 1, column - 1];
    D = [row + 1, column];
    DR = [row + 1, column + 1];
  } else if (row == gameSettings.Hl - 1 && column == 0) {
    UL = null;
    U = [row - 1, column];
    UR = [row - 1, column + 1];
    DL = null;
    D = null;
    DR = [gameSettings.Hl - 1, 1];
  } else if (row == gameSettings.Hl - 1 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 == 0) {
    UL = [row, column - 1];
    U = [row - 1, column];
    UR = null;
    DL = null;
    D = null;
    DR = null;
  } else if (row == gameSettings.Hl - 1 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 != 0) {
    UL = [row - 1, column - 1];
    U = [row - 1, column];
    UR = null;
    DL = [row, column - 1];
    D = null;
    DR = null;
  } else if (row == gameSettings.Hl - 1 && column % 2 != 0) {
    UL = [row - 1, column - 1];
    U = [row - 1, column];
    UR = [row - 1, column + 1];
    DL = [row, column - 1];
    D = null;
    DR = [row, column + 1];
  } else if (row == gameSettings.Hl - 1) {
    UL = [row, column - 1];
    U = [row - 1, column];
    UR = [row, column + 1];
    DL = null;
    D = null;
    DR = null;
  } else if (column == 0) {
    UL = null;
    U = [row - 1, 0];
    UR = [row - 1, column + 1];
    DL = null;
    D = [row + 1, 0];
    DR = [row, column + 1];
  } else if (column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 == 0) {
    UL = [row, gameSettings.Hw - 2];
    U = [row - 1, gameSettings.Hw - 1];
    UR = null;
    DL = [row + 1, gameSettings.Hw - 2];
    D = [row + 1, gameSettings.Hw - 1];
    DR = null;
  }
  else if (column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 != 0) {
    UL = [row + 1, gameSettings.Hw - 2];
    U = [row - 1, gameSettings.Hw - 1];
    UR = null;
    DL = [row, gameSettings.Hw - 2];
    D = [row + 1, gameSettings.Hw - 1];
    DR = null;
  } else if (column % 2 != 0) {
    UL = [row - 1, column - 1];
    U = [row - 1, column];
    UR = [row - 1, column + 1];
    DL = [row, column - 1];
    D = [row + 1, column];
    DR = [row, column + 1];
  } else {
    UL = [row, column - 1];
    U = [row - 1, column];
    UR = [row, column + 1];
    DL = [row + 1, column - 1];
    D = [row + 1, column];
    DR = [row + 1, column + 1];
  }

  var neigbors = [UL, U, UR, DR, D, DL];

  return neigbors;
}

function findNeighbors2(row, column, gameSettings) {
  var UL, U, UR, DL, D, DR;

  if (row == 0 && column == 0) {
    UL = null;
    U = null;
    UR = null;
    DL = null;
    D = [1, 0];
    DR = [0, 1];
  } else if (row == 0 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 != 0) {
    UL = [0, gameSettings.Hw - 2];
    U = null;
    UR = null;
    DL = [1, gameSettings.Hw - 2];
    D = [1, gameSettings.Hw - 1];
    DR = null;
  } else if (row == 0 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 == 0) {
    UL = null;
    U = null;
    UR = null;
    DL = [0, gameSettings.Hw - 2];
    D = [1, gameSettings.Hw - 1];
    DR = null;
  } else if (row == 0 && column % 2 == 0) {
    UL = null;
    U = null;
    UR = null;
    DL = [row, column - 1];
    D = [row + 1, column];
    DR = [row, column + 1];
  } else if (row == 0) {
    UL = [row, column - 1];
    U = null;
    UR = [row, column + 1];
    DL = [row + 1, column - 1];
    D = [row + 1, column];
    DR = [row + 1, column + 1];
  } else if (row == gameSettings.Hl - 1 && column == 0) {
    UL = null;
    U = [row - 1, column];
    UR = [row - 1, column + 1];
    DL = null;
    D = null;
    DR = [gameSettings.Hl - 1, 1];
  } else if (row == gameSettings.Hl - 1 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 != 0) {
    UL = [row, column - 1];
    U = [row - 1, column];
    UR = null;
    DL = null;
    D = null;
    DR = null;
  } else if (row == gameSettings.Hl - 1 && column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 == 0) {
    UL = [row - 1, column - 1];
    U = [row - 1, column];
    UR = null;
    DL = [row, column - 1];
    D = null;
    DR = null;
  } else if (row == gameSettings.Hl - 1 && column % 2 == 0) {
    UL = [row - 1, column - 1];
    U = [row - 1, column];
    UR = [row - 1, column + 1];
    DL = [row, column - 1];
    D = null;
    DR = [row, column + 1];
  } else if (row == gameSettings.Hl - 1) {
    UL = [row, column - 1];
    U = [row - 1, column];
    UR = [row, column + 1];
    DL = null;
    D = null;
    DR = null;
  } else if (column == 0) {
    UL = null;
    U = [row - 1, 0];
    UR = [row - 1, column + 1];
    DL = null;
    D = [row + 1, 0];
    DR = [row, column + 1];
  } else if (column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 != 0) {
    UL = [row, gameSettings.Hw - 2];
    U = [row - 1, gameSettings.Hw - 1];
    UR = null;
    DL = [row + 1, gameSettings.Hw - 2];
    D = [row + 1, gameSettings.Hw - 1];
    DR = null;
  }
  else if (column == gameSettings.Hw - 1 && (gameSettings.Hw - 1) % 2 == 0) {
    UL = [row + 1, gameSettings.Hw - 2];
    U = [row - 1, gameSettings.Hw - 1];
    UR = null;
    DL = [row, gameSettings.Hw - 2];
    D = [row + 1, gameSettings.Hw - 1];
    DR = null;
  } else if (column % 2 == 0) {
    UL = [row - 1, column - 1];
    U = [row - 1, column];
    UR = [row - 1, column + 1];
    DL = [row, column - 1];
    D = [row + 1, column];
    DR = [row, column + 1];
  } else {
    UL = [row, column - 1];
    U = [row - 1, column];
    UR = [row, column + 1];
    DL = [row + 1, column - 1];
    D = [row + 1, column];
    DR = [row + 1, column + 1];
  }

  var neigbors = [UL, U, UR, DR, D, DL];

  return neigbors;
}




//checks if certain origin hex is a valid choice
function isValidChoose(row, column, hive, gameSettings) {
  neighbors = findNeighbors(row, column, gameSettings);

  for (var i = 0; i < 6; i++) {
    if (neighbors[i] != null) {
      if (hive[neighbors[i][0]][neighbors[i][1]] == 1) {
        return false;
      }
    }
  }

  if ((neighbors[0] != null && neighbors[1] != null && hive[neighbors[0][0]][neighbors[0][1]] == 0 && hive[neighbors[1][0]][neighbors[1][1]] == 0) ||
    (neighbors[1] != null && neighbors[2] != null && hive[neighbors[1][0]][neighbors[1][1]] == 0 && hive[neighbors[2][0]][neighbors[2][1]] == 0) ||
    (neighbors[2] != null && neighbors[3] != null && hive[neighbors[2][0]][neighbors[2][1]] == 0 && hive[neighbors[3][0]][neighbors[3][1]] == 0) ||
    (neighbors[3] != null && neighbors[4] != null && hive[neighbors[3][0]][neighbors[3][1]] == 0 && hive[neighbors[4][0]][neighbors[4][1]] == 0) ||
    (neighbors[4] != null && neighbors[5] != null && hive[neighbors[4][0]][neighbors[4][1]] == 0 && hive[neighbors[5][0]][neighbors[5][1]] == 0) ||
    (neighbors[5] != null && neighbors[0] != null && hive[neighbors[5][0]][neighbors[5][1]] == 0 && hive[neighbors[0][0]][neighbors[0][1]] == 0)) {
    return true;
  }
  return false;
}

function mapping(gameSettings) {
  while (chnum != Math.round(gameSettings.Hl * gameSettings.Hw * gameSettings.CHrate)) {



    numCH = 0;
    hexCount = 0;
    submarine = true;
    gameSettings.Hw += 6;
    gameSettings.Hl += 6;

    var hive = [[]];

    //initialize the hive
    //0= didn't reach,1=CH,2= blocked from choosing

    for (i = 0; i < gameSettings.Hl; i++) {
      hive[i] = [];
      for (j = 0; j < gameSettings.Hw; j++) {
        hive[i][j] = 0;
      }
    }


    while (numCH < gameSettings.CHrate * gameSettings.Hl * gameSettings.Hw && hexCount < gameSettings.Hl * gameSettings.Hw) {


      //origin yellow hex choose
      blocked = true;

      while (blocked) {
        //random for origin yellow hex
        CHoneRow = Math.floor((Math.random() * gameSettings.Hl));
        CHoneColumn = Math.floor((Math.random() * gameSettings.Hw));

        if (hive[CHoneRow][CHoneColumn] == 0) {
          if (isValidChoose(CHoneRow, CHoneColumn, hive, gameSettings)) {
            //yellow hive selected in the mapping
            hive[CHoneRow][CHoneColumn] = 1;
            numCH++;
            blocked = false;
            hexCount++;
            submarine = true;
          } else {
            hive[CHoneRow][CHoneColumn] = 2;
            hexCount++;
            // blocked = false;
            submarine = false;
          }
        }
      }

      if (!submarine) {
        //console.log("first cancel");
        continue;
      }

      //hex's neighbors
      neighbors = findNeighbors(CHoneRow, CHoneColumn, gameSettings);
      hexOne = [CHoneRow, CHoneColumn];

      //console.log(hexOne);
      //console.log(neighbors);

      //random choice between origin hex's neighbors

      selected = false;
      secondChoiceCount = 0;// counts the number of choices attempts in the while below
      while (!selected) {
        choice = Math.floor((Math.random() * 6));
        if (secondChoiceCount >= 500) {
          submarine = false;
          numCH -= 1;
          selected = true;
          hive[hexOne[0]][hexOne[1]] = 2;
          hexCount++;
        } else if (neighbors[choice] != null) {
          if (hive[neighbors[choice][0]][neighbors[choice][1]] == 0) {
            hive[neighbors[choice][0]][neighbors[choice][1]] = 1;
            numCH++;
            selected = true;

            submarine = true;
          }
        }
        secondChoiceCount++;
      }


      if (!submarine) {
        //console.log("second cancel");
        continue;
      }

      hexTwo = neighbors[choice];
      neighborsTwo = findNeighbors(hexTwo[0], hexTwo[1], gameSettings);

      //console.log(hexTwo);
      //console.log(neighborsTwo);


      //choose the second yellow neighbor

      selectedTwo = false;
      hexThree = [];


      while (!selectedTwo) {
        choiceTwo = Math.floor((Math.random() * 2));
        //0=choose counter clockwise neighbor,1=choose clockwise neighbor
        if (choice == 0 && choiceTwo == 0) {
          if (neighbors[5] != null && hive[neighbors[5][0]][neighbors[5][1]] == 0) {
            hive[neighbors[5][0]][neighbors[5][1]] = 1;
            numCH++;
            hexThree = [neighbors[5][0], neighbors[5][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[5] != null && neighbors[1] != null && hive[neighbors[5][0]][neighbors[5][1]] == 2 && hive[neighbors[1][0]][neighbors[1][1]] == 0) {
            hive[neighbors[1][0]][neighbors[1][1]] = 1;
            numCH++;
            hexThree = [neighbors[1][0], neighbors[1][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[5] == null && neighbors[1] != null && hive[neighbors[1][0]][neighbors[1][1]] == 0) {
            hive[neighbors[1][0]][neighbors[1][1]] = 1;
            numCH++;
            hexThree = [neighbors[1][0], neighbors[1][1]];
            selectedTwo = true;
            hexCount++;
          } else {
            submarine = false;
            numCH -= 2;
            selectedTwo = true;
            hive[hexOne[0]][hexOne[1]] = 2;
            hive[hexTwo[0]][hexTwo[1]] = 2;
            hexCount += 2;

          }
        } else if (choice == 0 && choiceTwo == 1) {
          if (neighbors[1] != null && hive[neighbors[1][0]][neighbors[1][1]] == 0) {
            hive[neighbors[1][0]][neighbors[1][1]] = 1;
            numCH++;
            hexThree = [neighbors[1][0], neighbors[1][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[1] != null && neighbors[5] != null && hive[neighbors[1][0]][neighbors[1][1]] == 2 && hive[neighbors[5][0]][neighbors[5][1]] == 0) {
            hive[neighbors[5][0]][neighbors[5][1]] = 1;
            numCH++;
            hexThree = [neighbors[5][0], neighbors[5][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[1] == null && neighbors[5] != null && hive[neighbors[5][0]][neighbors[5][1]] == 0) {
            hive[neighbors[5][0]][neighbors[5][1]] = 1;
            numCH++;
            hexThree = [neighbors[5][0], neighbors[5][1]];
            selectedTwo = true;
            hexCount++;
          } else {
            submarine = false;
            numCH -= 2;
            selectedTwo = true;
            hive[hexOne[0]][hexOne[1]] = 2;
            hive[hexTwo[0]][hexTwo[1]] = 2;
            hexCount += 2;

          }
        } else if (choice == 5 && choiceTwo == 0) {
          if (neighbors[4] != null && hive[neighbors[4][0]][neighbors[4][1]] == 0) {
            hive[neighbors[4][0]][neighbors[4][1]] = 1;
            numCH++;
            hexThree = [neighbors[4][0], neighbors[4][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[4] != null && neighbors[0] != null && hive[neighbors[4][0]][neighbors[4][1]] == 2 && hive[neighbors[0][0]][neighbors[0][1]] == 0) {
            hive[neighbors[0][0]][neighbors[0][1]] = 1;
            numCH++;
            hexThree = [neighbors[0][0], neighbors[0][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[4] == null && neighbors[0] != null && hive[neighbors[0][0]][neighbors[0][1]] == 0) {
            hive[neighbors[0][0]][neighbors[0][1]] = 1;
            numCH++;
            hexThree = [neighbors[0][0], neighbors[0][1]];
            selectedTwo = true;
            hexCount++;
          } else {
            submarine = false;
            numCH -= 2;
            selectedTwo = true;
            hive[hexOne[0]][hexOne[1]] = 2;
            hive[hexTwo[0]][hexTwo[1]] = 2;
            hexCount += 2;

          }
        } else if (choice == 5 && choiceTwo == 1) {
          if (neighbors[0] != null && hive[neighbors[0][0]][neighbors[0][1]] == 0) {
            hive[neighbors[0][0]][neighbors[0][1]] = 1;
            numCH++;
            hexThree = [neighbors[0][0], neighbors[0][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[0] != null && neighbors[4] != null && hive[neighbors[0][0]][neighbors[0][1]] == 2 && hive[neighbors[4][0]][neighbors[4][1]] == 0) {
            hive[neighbors[4][0]][neighbors[4][1]] = 1;
            numCH++;
            hexThree = [neighbors[4][0], neighbors[4][1]];
            selectedTwo = true;
            hexCount++;
          } else if (neighbors[0] == null && neighbors[4] != null && hive[neighbors[4][0]][neighbors[4][1]] == 0) {
            hive[neighbors[4][0]][neighbors[4][1]] = 1;
            numCH++;
            hexThree = [neighbors[4][0], neighbors[4][1]];
            selectedTwo = true;
            hexCount++;
          } else {
            submarine = false;
            numCH -= 2;
            selectedTwo = true;
            hive[hexOne[0]][hexOne[1]] = 2;
            hive[hexTwo[0]][hexTwo[1]] = 2;
            hexCount += 2;

          }
        } else if (choiceTwo == 0 && neighbors[choice - 1] != null && hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] == 0) {
          hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] = 1;
          numCH++;
          hexThree = [neighbors[choice - 1][0], neighbors[choice - 1][1]];
          selectedTwo = true;
          hexCount++;
        } else if (choiceTwo == 0 && neighbors[choice + 1] != null && neighbors[choice - 1] != null && hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] == 2 && hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] == 0) {
          hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] = 1;
          numCH++;
          hexThree = [neighbors[choice + 1][0], neighbors[choice + 1][1]];
          selectedTwo = true;
          hexCount++;

        } else if (choiceTwo == 0 && neighbors[choice + 1] != null && neighbors[choice - 1] == null && hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] == 0) {
          hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] = 1;
          numCH++;
          hexThree = [neighbors[choice + 1][0], neighbors[choice + 1][1]];
          selectedTwo = true;
          hexCount++;

        } else if (choiceTwo == 1 && neighbors[choice + 1] != null && hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] == 0) {
          hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] = 1;
          numCH++;
          hexThree = [neighbors[choice + 1][0], neighbors[choice + 1][1]];
          selectedTwo = true;
          hexCount++;
        } else if (choiceTwo == 1 && neighbors[choice - 1] != null && neighbors[choice + 1] != null && hive[neighbors[choice + 1][0]][neighbors[choice + 1][1]] == 2 && hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] == 0) {
          hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] = 1;
          numCH++;
          hexThree = [neighbors[choice - 1][0], neighbors[choice - 1][1]];
          selectedTwo = true;
          hexCount++;

        } else if (choiceTwo == 1 && neighbors[choice - 1] != null && neighbors[choice + 1] == null && hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] == 0) {
          hive[neighbors[choice - 1][0]][neighbors[choice - 1][1]] = 1;
          numCH++;
          hexThree = [neighbors[choice - 1][0], neighbors[choice - 1][1]];
          selectedTwo = true;
          hexCount++;

        } else {
          submarine = false;
          numCH -= 2;
          selectedTwo = true;
          hive[hexOne[0]][hexOne[1]] = 2;
          hive[hexTwo[0]][hexTwo[1]] = 2;
          hexCount += 2;

        }

      }

      //Blocking The neighbors of the three yellow hexes

      if (!submarine) {
        //console.log("third cancel");
        continue;
      }

      neighborsThree = findNeighbors(hexThree[0], hexThree[1], gameSettings);

      //console.log(hexThree);
      //console.log(neighborsThree);


      for (l = 0; l < 6; l++) {
        if (neighbors[l] != null) {
          if (hive[neighbors[l][0]][neighbors[l][1]] != 1) {
            hive[neighbors[l][0]][neighbors[l][1]] = 2;
            hexCount++;
          }
        }
        if (neighborsTwo[l] != null) {
          if (hive[neighborsTwo[l][0]][neighborsTwo[l][1]] != 1) {
            hive[neighborsTwo[l][0]][neighborsTwo[l][1]] = 2;
            hexCount++;
          }
        }
        if (neighborsThree[l] != null) {
          if (hive[neighborsThree[l][0]][neighborsThree[l][1]] != 1) {
            hive[neighborsThree[l][0]][neighborsThree[l][1]] = 2;
            hexCount++;
          }
        }
      }





      submarine = true;


    }
    // changing 2 to 0

    for (k = 0; k < gameSettings.Hl; k++) {
      for (t = 0; t < gameSettings.Hw; t++) {
        if (hive[k][t] == 2) {
          hive[k][t] = 0;
        }
      }
    }
    //console.log(numCH);



    var finalHive = [[]];

    for (i = 0; i < gameSettings.Hl - 6; i++) {
      finalHive[i] = [];
      for (j = 0; j < gameSettings.Hw - 6; j++) {
        finalHive[i][j] = 0;
      }
    }

    var
      rowCount = 0,
      colCount = 0;

    for (i = 3; i < gameSettings.Hl - 3; i++) {
      for (j = 3; j < gameSettings.Hw - 3; j++) {

        finalHive[rowCount][colCount] = hive[i][j];
        colCount++;
      }
      colCount = 0;
      rowCount++;
    }



    // console.log("Grid mapping:");
    // console.log(" ");

    // for (k = 0; k < gameSettings.Hl; k++) {
    //   arrText = '';
    //   for (t = 0; t < gameSettings.Hw; t++) {
    //     arrText += hive[k][t] + '';
    //   }
    //   console.log(arrText);
    // }

    console.log("Grid mapping:");
    console.log(" ");
    var chnum = 0;
    for (k = 0; k < gameSettings.Hl - 6; k++) {
      arrText = '';
      for (t = 0; t < gameSettings.Hw - 6; t++) {
        arrText += finalHive[k][t] + '';
        if (finalHive[k][t]) {
          chnum++;
        }
      }
      console.log(arrText);
    }
    console.log("ch num:");
    console.log(chnum);
    gameSettings.Hw -= 6;
    gameSettings.Hl -= 6;
  }

  return finalHive;
}

