
const express = require('express');
const exphbs = require('express-handlebars');
var socket = require('socket.io')
const app = express();
const querystring = require('querystring');
const mapping = require('./mapping');
var fs = require('fs');
var serveIndex = require('serve-index');


var hbs = exphbs.create({
  helpers: {
    ifEqual: function (val1, val2, options) {
      return (val1 === val2) ? options.fn(this) : options.inverse(this);
    },
    ifNotEqueal: function (val1, val2, options) {
      return (val1 !== val2) ? options.fn(this) : options.inverse(this);
    }
  },
  defaultLayout: 'main'
});



//Handlebars Middleware

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.use(express.static('public'));
app.use('/data', serveIndex('public/data', { icons: true }));

const port = 3000;
// var server = app.listen(process.env.PORT , '127.0.0.1', () => {
//   console.log(`Server started on port `);
// });
var server = app.listen(process.env.PORT || 3000);


//Index Route
app.get('/', (req, res) => {
  console.log('game: ' + gameSettings.currentGame);
  console.log('round: ' + gameSettings.currentRound);
  if (gameSettings.currentRound > 1 || gameSettings.currentGame > 1) {
    res.send("Can't join, game already started.");
  }
  else {
    res.render('index', { settings: gameSettings });
  }
});

// app.post('/clicked', (req, res) => {
//   io.sockets.emit('clicked');
// })

//Admin route
app.get('/admin', (req, res) => {
  res.render('admin', { title: 'Settings', settings: gameSettings });
});

app.get('/instructions', (req, res) => {
  res.render('instructions', { title: 'Instructions', settings: gameSettings });
});

app.get('/map', (req, res) => {
  res.render('map', { title: 'Map', Hw: encodeURIComponent(JSON.stringify(gameSettings.Hw)), Hl: encodeURIComponent(JSON.stringify(gameSettings.Hl)), grid: encodeURIComponent(JSON.stringify(map)) });
});

app.get('/settings', (req, res) => {
  res.send(gameSettings);
});
//update settings according to the admin page
app.post('/admin', (req, res) => {
  let body = '';
  req.on('data', chunk => {
    body += chunk.toString(); // convert Buffer to string
  });
  // updates the settings according to data sent from the admin page
  req.on('end', () => {
    var data = querystring.parse(body);
    gameSettings.Ngames = Number(data.Ngames);
    gameSettings.Nrounds = Number(data.Nrounds);
    gameSettings.Nplayers = Number(data.Nplayers);
    gameSettings.Nsquares = Number(data.Nsquares);
    gameSettings.Hl = Number(data.Hl);
    gameSettings.Hw = Number(data.Hw);
    gameSettings.CHrate = Number(data.CHrate);
    gameSettings.minCost = Number(data.minCost);
    gameSettings.maxCost = Number(data.maxCost);
    gameSettings.payoff = Number(data.payoff);
    gameSettings.extraPayoff = Number(data.extraPayoff);
    gameSettings.currency = data.currency;
    gameSettings.exchangeRate = Number(data.exchangeRate);
    gameSettings.showUp = Number(data.showUp);
    gameSettings.timeoutTime = Number(data.timeoutTime);
    gameSettings.timeoutPenalty = Number(data.timeoutPenalty);
    gameSettings.timeoutKick = Number(data.timeoutKick);
    gameSettings.messageTime = Number(data.messageTime);
    gameSettings.gameMode = data.mode;
    gameSettings.mapNumber = data.mapNumber;
    setGameMode(data.mode);
    resetGame();
    io.sockets.emit('restart');
    res.sendStatus(200);
  });
});

app.post('/map', (req, res) => {
  let body = '';
  req.on('data', chunk => {
    body += chunk.toString(); // convert Buffer to string
  });
  // updates the current map according to admin changings
  req.on('end', () => {
    var data = querystring.parse(body);
    map[Number(data.hexRow)][Number(data.hexCol)] = Number(data.newColor);
    console.log("updated map:");
    for (k = 0; k < gameSettings.Hl; k++) {
      arrText = '';
      for (t = 0; t < gameSettings.Hw; t++) {
        arrText += map[k][t] + '';
      }
      console.log(arrText);
    }
    res.sendStatus(200);
  });
});


//socket & game variables setup
var io = socket(server),
  subjects = {},
  non_singleton_subjects = {},
  disconnected_non_singleton_subjects = {},
  singleton_subjects = {},
  disconnected_singleton_subjects = {},
  subjectsOutputString = {},
  rootHexs = {},
  IDOpen1 = {},
  IDOpen1ToAssign = {},
  IDOpen2 = {},
  IDOpen2ToAssign = {},
  IDOpen3 = {},
  IDOpen3ToAssign = {},
  IDSelf = {},
  IDSelfToAssign = {},
  IDCounter = 0,
  patent_submarine_owners = {},
  yellow_hex_owners_tradesecrets = {},
  tradeSecretsHexOwners = {},
  singleTradeOutput = {},
  disconnected = false,
  payoffsCollection = {},
  maps = {},
  costs = [],

  gameSettings = {
    Ngames: 1,
    Nrounds: 3,
    Nplayers: 1,
    Nsquares: 18,
    NClicked: 0,
    currentGame: 1,
    currentRound: 1,
    Hl: 30,
    Hw: 70,
    CHrate: 0.05,
    minCost: 10,
    maxCost: 30,
    cost: 1,
    payoff: 60,
    extraPayoff: 300,
    skipPay: 0,
    currency: '&#8362;',
    exchangeRate: 10,
    showUp: 20,
    timeoutTime: 500,
    timeoutPenalty: 10,
    timeoutKick: 3,
    messageTime: 2.5,
    gameMode: 'Singleton',
    mapNumber: 1,
    currMap: 0,
  },
  date = getDateString(),
  path1 = 'public/data/' + date + "M-Singleton",
  path2 = 'public/data/' + date + "M-" + gameSettings.gameMode,
  path = 'public/data/';
gameSettings.currMap = gameSettings.mapNumber;

function createMap(mapNum) {
  //initialize a map
  var map = [[]];

  //initialize the hive
  //0= didn't reach,1=CH,2= blocked from choosing

  for (i = 0; i < gameSettings.Hl; i++) {
    map[i] = [];
    for (j = 0; j < gameSettings.Hw; j++) {
      map[i][j] = 0;
    }
  }


  var mapPath = "public/data/maps5/" + mapNum + ".txt";
  var text = fs.readFileSync(mapPath, "utf8");
  console.log("Text 1:");
  console.log(text);
  var textByLine = text.split('\n');

  for (var row in textByLine) {
    if (row < textByLine.length - 1) {
      textByLine[row] = textByLine[row].substring(0, textByLine[row].length - 1);
    }

    for (var i = 0; i < textByLine[row].length; i++) {
      map[row][i] = Number(textByLine[row].charAt(i));
    }
  }

  console.log("Text 1 after substring /r:");
  console.log(textByLine);
  var numch = 0;

  console.log("map ", mapNum, ":");
  for (k = 0; k < gameSettings.Hl; k++) {
    arrText = '';
    for (t = 0; t < gameSettings.Hw; t++) {
      arrText += map[k][t] + '';
      if (map[k][t]) {
        numch++;
      }
    }
    console.log(arrText);
  }

  console.log("ch num:");
  console.log(numch);

  return map;
}




map = createMap(gameSettings.mapNumber);
// map = mapping.mapping(gameSettings);
ready = 0;
started = false;
gameAnswer = singletonAnswer;


function generateCostForNewRound() {
  // gameSettings.cost = gameSettings.minCost + Math.floor((Math.random() * (gameSettings.maxCost - gameSettings.minCost)));
  let costsArray = [5, 10, 15, 20, 25, 30, 35];
  let pickedCost = Math.round(Math.random() * 6);
  gameSettings.cost = costsArray[pickedCost];
}



io.on('connection', function (s) {
  // new player joins
  s.on('new subject', function (data, callback) {
    if (data.subjectID in subjects) {
      callback(false, 'ID');
    } else if (started) {
      callback(false, 'STARTED');
    } else {
      // the player joined succesfully
      //adds information to the socket
      data.socket = s;
      // additional info on the socket
      s.subjectID = data.subjectID
      s.timeouts = 0;
      s.payoffs = [];
      payoffsCollection[s.id] = [];
      subjects[s.subjectID] = data;
      // if enough players alraedy joined, this player will use singleton mode
      if (Object.keys(non_singleton_subjects).length < gameSettings.Nplayers) {
        non_singleton_subjects[s.subjectID] = data;
        callback({ singleton: false });
      } else {
        singleton_subjects[s.subjectID] = data;
        callback({ singleton: true });

        singleTradeOutput[s.id] =
          { IDopen1TD: {}, IDopen1ToAssignTD: {}, IDopen2TD: {}, IDopen2ToAssignTD: {}, IDopen3TD: {}, IDopen3ToAssignTD: {}, IDSelfTD: [], IDSelfToAssignTD: [] };



        subjectsOutputString[s.subjectID] =

          ["Hl", "Hw", "P", "Cmin", "Cmax", "C", "V", "VX", "Condition", "NG", "NR", "Nplayers", "ID", "age", "gender", "G", "MAP", "R", "NOpenDiscoveries1", "NOpenDiscoveries2", "NOpenDiscoveries3", "IDOpenDiscoveries1", "IDOpenDiscoveries2", "IDOpenDiscoveries3", "IDSelfOpenDiscoveries", "Hive", "Hexagon", "Order", "Extra", "Yellow", "Red", "Payoff"]
          + "\n";

      }


    }
  });

  //user disconnection
  s.on('disconnect', (data) => {

    //write reward line for disconnected player

    if (started && (s.id in payoffsCollection)) {
      singleRewardLineInOutput(s.id, s.subjectID);
    }


    id = s.subjectID;
    if (id) {
      delete subjects[id];
      if (id in non_singleton_subjects) {
        disconnected_non_singleton_subjects[id] = 1;
        delete non_singleton_subjects[id];
      } else {
        disconnected_singleton_subjects[id] = 1;
        delete singleton_subjects[id];
      }


      //if the disconnected player was the only player who didn't send his choice at the current round => trigger the round handler

      if (Object.keys(subjects).length == 0) {
        console.log("no players connected");
        resetGame();
      }

      if (!(id in choices) && Object.keys(choices).length == Object.keys(subjects).length && Object.keys(choices).length > 0) {
        disconnected = true;
        handleRoundWrapper();
      }


    }
  });

  // user finished reading instruction
  s.on('ready', () => {

    ready++;
    let nonSingletonNum = Object.keys(non_singleton_subjects).length;
    let singletonNum = Object.keys(singleton_subjects).length;

    // all players are ready, start game
    // if (gameSettings.Nplayers == ready) {
    if ((gameSettings.Nplayers == nonSingletonNum) && (nonSingletonNum + singletonNum == ready)) {
      started = true;

      // generateCostForNewRound();
      // costs.push(gameSettings.cost);

      io.sockets.emit('start', gameSettings);
      ready = 0;

      if (gameSettings.gameMode == "Trade Secrets" || gameSettings.gameMode == "Singleton") {

        console.log("singleton or trade secrets?");
        console.log(gameSettings.gameMode == "Trade Secrets" || gameSettings.gameMode == "Singleton");

        for (sub in subjects) {
          console.log("subjects[sub].socket.id:");
          console.log(subjects[sub].socket.id);
          singleTradeOutput[subjects[sub].socket.id] =
            { IDopen1TD: {}, IDopen1ToAssignTD: {}, IDopen2TD: {}, IDopen2ToAssignTD: {}, IDopen3TD: {}, IDopen3ToAssignTD: {}, IDSelfTD: [], IDSelfToAssignTD: [] };
        }
      }

      //initalizing each players map of red and yellow for use of output data
      for (sub in subjects) {
        maps[subjects[sub].socket.id] = [[]];

        for (i = 0; i < gameSettings.Hl; i++) {
          maps[subjects[sub].socket.id][i] = [];
          for (j = 0; j < gameSettings.Hw; j++) {
            maps[subjects[sub].socket.id][i][j] = -1;//-1 set to not yellow nor red
          }
        }
      }

      //

      for (sub in non_singleton_subjects) {
        var
          identication = subjects[sub].inputLine["ID"],
          age = subjects[sub].inputLine["age"],
          gender = subjects[sub].inputLine["gender"];

        subjectsOutputString[sub] =

          ["Hl", "Hw", "P", "Cmin", "Cmax", "C", "V", "VX", "Condition", "NG", "NR", "Nplayers", "ID", "age", "gender", "G", "MAP", "R", "NOpenDiscoveries1", "NOpenDiscoveries2", "NOpenDiscoveries3", "IDOpenDiscoveries1", "IDOpenDiscoveries2", "IDOpenDiscoveries3", "IDSelfOpenDiscoveries", "Hive", "Hexagon", "Order", "Extra", "Yellow", "Red", "Payoff"]
          + "\n";
      }
    }
    // update the amount of players that are waiting
    else {
      io.sockets.emit('waiting', singletonNum + nonSingletonNum - ready);
    }
  });

  choices = {}

  s.on('play', (data) => {
    //adds the choice to a dictionary

    data.subjectID = s.subjectID;
    subjects[s.subjectID].inputLine["costs"].push(data.currentCost);
    subjects[s.subjectID].inputLine["lastRoundCost"] = data.currentCost;


    if (data.x == -1) {
      subjects[s.subjectID].inputLine["Hive"] = 0;
      subjects[s.subjectID].inputLine["Hexagon"] = ".";
      subjects[s.subjectID].inputLine["index"] = ".";
    } else if (data.x == -2) {
      subjects[s.subjectID].inputLine["Hive"] = -1;
      subjects[s.subjectID].inputLine["Hexagon"] = ".";
      subjects[s.subjectID].inputLine["index"] = ".";
      subjects[s.subjectID].inputLine["Payoff"] = -gameSettings.timeoutPenalty;
    } else {
      subjects[s.subjectID].inputLine["Hive"] = map[data.x][data.y] ? 2 : 1;
      var hexagonString = "(" + data.x + "-" + data.y + ")";
      hexagonString = hexagonString.toString();
      subjects[s.subjectID].inputLine["Hexagon"] = "(" + data.x + "-" + data.y + ")";
      subjects[s.subjectID].inputLine["index"] = [data.x, data.y];
    }

    choices[s.id] = data;
    //when all players send their choice => trigger round handler
    if (Object.keys(choices).length == Object.keys(subjects).length) {
      handleRoundWrapper();
      //end of a game
      if (gameSettings.currentRound - 1 == gameSettings.Nrounds) {
        //end of all games
        if (gameSettings.currentGame == gameSettings.Ngames) {
          handleFinishedAll();
          //update game and round counters, reset map.
        }
        else {
          // new game
          gameSettings.currentGame++;
          gameSettings.currentRound = 1;
          // map = mapping.mapping(gameSettings);
          var newMap = Math.floor((Math.random() * 10) + 1);
          gameSettings.currMap = newMap;
          map = createMap(newMap);
          generateCostForNewRound();
          costs.push(gameSettings.cost);
          IDOpen1 = {};
          rootHexs = {};
          IDOpen1ToAssign = {};
          IDOpen2 = {};
          IDOpen2ToAssign = {};
          IDOpen3 = {};
          IDOpen3ToAssign = {};
          IDSelf = {};
          IDSelfToAssign = {};
          IDCounter = 0,

            patent_submarine_owners = {};
          yellow_hex_owners_tradesecrets = {};
          tradeSecretsHexOwners = {};



          if (gameSettings.gameMode == "Trade Secrets" || gameSettings.gameMode == "Singleton" || Object.keys(singleton_subjects).length > 0) {

            if (gameSettings.gameMode == "Trade Secrets" || gameSettings.gameMode == "Singleton") {
              for (sub in subjects) {
                singleTradeOutput[subjects[sub].socket.id] =
                  { IDopen1TD: {}, IDopen1ToAssignTD: {}, IDopen2TD: {}, IDopen2ToAssignTD: {}, IDopen3TD: {}, IDopen3ToAssignTD: {}, IDSelfTD: [], IDSelfToAssignTD: [] };
              }
            } else {//Object.keys(singleton_subjects).length > 0
              for (sub in singleton_subjects) {
                singleTradeOutput[subjects[sub].socket.id] =
                  { IDopen1TD: {}, IDopen1ToAssignTD: {}, IDopen2TD: {}, IDopen2ToAssignTD: {}, IDopen3TD: {}, IDopen3ToAssignTD: {}, IDSelfTD: [], IDSelfToAssignTD: [] };
              }
            }
          }


          io.sockets.emit('next-game', { game: gameSettings.currentGame - 1, rounds: gameSettings.Nrounds, cost: gameSettings.cost });
        }
      }
    }
  });
});



function handleRoundWrapper() {

  var answers = handleRound(choices, map);
  //update inputLine for the csv output file
  var pay, decimal;
  for (answer in answers) {
    pay = answers[answer].payoff = answers[answer].payoff;
    decimal = (pay - Math.floor(pay)) !== 0;
    if (decimal) {
      answers[answer].payoff = answers[answer].payoff.toFixed(1);
    }
  }

  UpdateInputLine(answers);
  writeToFile();

  // updating each player's map of yellow and red after current round results

  for (answer in answers) {
    console.log("player's id(answer in answers):");
    console.log("printing player's hexs to color(after write to file):");
    console.log(answers[answer].hexs);

    for (var i in answers[answer].hexs) {
      tuple = i.split(',');
      var hexRow = tuple[0];
      var hexCol = tuple[1];
      if (hexRow != -1 && hexRow != -2) {
        if (answers[answer].hexs[i]) {
          if (answers[answer].hexs[i] == 'others') {
            // hex.classList.add('foundCorrect');
            maps[answer][tuple[0]][tuple[1]] = 0;
            subjects[io.sockets.connected[answer].subjectID].inputLine["Red"]++;


            console.log("answer:");
            console.log(answer);
            console.log("putting 0 in player's map");
          }
          else {
            // hex.classList.add('clickedCorrect');
            console.log("answer:");
            console.log(answer);
            console.log("putting 1 in player's map");

            maps[answer][tuple[0]][tuple[1]] = 1;
            subjects[io.sockets.connected[answer].subjectID].inputLine["Yellow"]++;


          }
        }
        else {
          // hex.classList.add('clickedWrong');
          console.log("answer:");
          console.log(answer);
          console.log("leaving -1 in player's map(cause all wrong)");
        }
      }
    }
  }

  console.log("printing all maps dict to see how structured:");
  console.log("length of maps:");
  console.log(Object.keys(maps).length);
  console.log("print the maps keys:");
  console.log(Object.keys(maps));




  gameSettings.currentRound++;

  sendAnswersBack(answers);
  choices = {};
}



function handleRound(choices, map) {
  var non_singleton_choices = {};
  var singleton_choices = {};
  // iterates over the choices of all players
  for (var socket in choices) {
    /* if the player is at non-singleton mode need to
       take into consideration other player's choices */
    if (choices[socket].subjectID in non_singleton_subjects) {
      // tuple represents a chosen hexagon in the map
      // creates a dict with players choices where hexagon's position is the key

      tuple = [choices[socket].x, choices[socket].y];
      if (tuple in non_singleton_choices) {
        non_singleton_choices[tuple].push(socket);
      }
      else {
        non_singleton_choices[tuple] = [socket];
      }
    }
    else {
      // a singleton player
      tuple = [choices[socket].x, choices[socket].y];
      if (tuple in singleton_choices) {
        singleton_choices[tuple].push(socket);
      }
      else {
        singleton_choices[tuple] = [socket];
      }
    }
  }
  // calculate answers for each kind of player
  var singleton_answers = singletonAnswer(singleton_choices, map);
  var non_singleton_answers = gameAnswer(non_singleton_choices, map);
  //merge the dictionaries
  var answers = Object.assign({}, singleton_answers, non_singleton_answers);


  console.log('FINISHED');
  kick = updateTimeouts(choices);
  //kick players who reached the timeout limit
  for (player in kick) {
    if (kick[player] in answers) {
      delete answers[kick[player]];
    }
  }
  return answers;
}

function singleRewardLineInOutput(socket, ID) {
  // chosenStep = Math.floor(Math.random() * io.sockets.connected[socket].payoffs.length);
  chosenStep = Math.floor(Math.random() * payoffsCollection[socket].length);
  console.log("chosenStep:");
  console.log(chosenStep);
  // chosenValue = (io.sockets.connected[socket].payoffs[chosenStep]);
  chosenValue = (payoffsCollection[socket][chosenStep]);
  chosenCost = subjects[ID].inputLine["costs"][chosenStep];
  // chosenCost = costs[chosenStep];
  console.log("chosenValue:");
  console.log(chosenValue);
  subjects[ID].inputLine["reward"] = chosenValue - chosenCost;

  var reward = subjects[ID].inputLine["reward"];
  var dateToFile = new Date().toJSON().slice(0, 19).replace('T', ' ');
  subjectsOutputString[ID] += "\n" + ["Date", dateToFile] + "\n" + "\n" + [

    "Show up", gameSettings.showUp
  ] + "\n" + ["Reward", reward] + "\n" + ["Total", gameSettings.showUp + Math.round((reward / gameSettings.exchangeRate))];

  singleWriteToFile(ID);

}

function singleWriteToFile(ID) {

  if (Object.keys(singleton_subjects).length > 0) {
    console.log(path1);
    if (!fs.existsSync(path1)) {
      fs.mkdirSync(path1);
    }
  }

  console.log(path2);
  if (!fs.existsSync(path2)) {
    fs.mkdirSync(path2);
  }


  if (!(ID in disconnected_non_singleton_subjects) && !(ID in disconnected_singleton_subjects)) {
    if (ID in non_singleton_subjects) {
      name = path2 + '/' + ID + '.csv';
      var ws = fs.createWriteStream(name);
      ws.write(subjectsOutputString[ID]);
    } else if (ID in singleton_subjects) {
      name = path1 + '/' + ID + '.csv';
      var ws = fs.createWriteStream(name);
      ws.write(subjectsOutputString[ID]);
    }
  }



}


// chooses a random round for each player for reward purposes
function handleFinishedAll() {
  for (var socket in io.sockets.connected) {
    if ('payoffs' in io.sockets.connected[socket]) {
      // chose a random step and send it and it's payoff
      chosenStep = Math.floor(Math.random() * io.sockets.connected[socket].payoffs.length);
      console.log("chosenStep:");
      console.log(chosenStep);
      chosenValue = (io.sockets.connected[socket].payoffs[chosenStep]);
      // chosenCost = costs[chosenStep];
      chosenCost = subjects[io.sockets.connected[socket].subjectID].inputLine["costs"][chosenStep];
      console.log("chosenValue:");
      console.log(chosenValue);
      subjects[io.sockets.connected[socket].subjectID].inputLine["reward"] = chosenValue - chosenCost;

      var reward = subjects[io.sockets.connected[socket].subjectID].inputLine["reward"];
      var dateToFile = new Date().toJSON().slice(0, 19).replace('T', ' ');
      subjectsOutputString[io.sockets.connected[socket].subjectID] += "\n" + ["Date", dateToFile] + "\n" + "\n" + [

        "Show up", gameSettings.showUp
      ] + "\n" + ["Reward", reward] + "\n" + ["Total", gameSettings.showUp + Math.round((reward / gameSettings.exchangeRate))];
      writeToFile();
      io.sockets.connected[socket].emit('finished-all', { step: chosenStep, value: chosenValue - chosenCost });



      setTimeout(() => {
        resetGame();
      }, 3000);
    }
  }
}

// sends answer to each player (at the end of each round)
function sendAnswersBack(answers) {
  // generateCostForNewRound();
  // costs.push(gameSettings.cost);
  for (var socket in answers) {
    if ('payoffs' in io.sockets.connected[socket]) {
      io.sockets.connected[socket].emit('next-round', { game: gameSettings.currentGame, round: gameSettings.currentRound, cost: gameSettings.cost, answer: answers[socket] });
      //adds the current payoff to all previous payoffs of the player
      io.sockets.connected[socket].payoffs.push(answers[socket].payoff);
      payoffsCollection[socket].push(answers[socket].payoff);
    }
  }
}

// changes the game mode
function setGameMode(mode) {
  switch (mode) {
    case 'Patent':
      gameAnswer = patentAnswer;
      break;
    case 'No Patent':
      gameAnswer = noPatentAnswer;
      break;
    case 'Trade Secrets':
      gameAnswer = tradeSecretsAnswer;
      break;
    default:
      gameAnswer = singletonAnswer;
  }
}

function singletonAnswer(choices, map) {
  console.log('-- singleton answer--');
  console.log("singleton_subjects:");
  console.log(singleton_subjects);
  var answers = {};
  for (var choice in choices) {
    console.log("choice:");
    console.log(choice);
    console.log("choices[choice]:");
    console.log(choices[choice]);
    for (var player in choices[choice]) {
      console.log("choices[choice][player]:");
      console.log(choices[choice][player]);
      var xy = choice.split(',');
      var payoff;
      var mapValue = 0;
      // square
      if (xy[0] == -1) {
        payoff = skipPayoff();
        subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = ".";
        subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = ".";
      }
      // timeout
      else if (xy[0] == -2) {
        payoff = -gameSettings.timeoutPenalty;
        subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = ".";
        subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = ".";
      }
      else {
        mapValue = map[xy[0]][xy[1]];

        if (mapValue) {
          //Updates for output file
          submarine = [choice];

          newOwnerNeighbors = mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings);


          console.log("singleTradeOutput[choices[choice][player]][IDSelfToAssignTD]:");
          console.log(singleTradeOutput[choices[choice][player]]["IDSelfToAssignTD"]);

          singleTradeOutput[choices[choice][player]]["IDSelfToAssignTD"].push(choice);

          console.log("singleTradeOutput[choices[choice][player]][IDSelfToAssignTD] after push new yellow hex:");
          console.log(singleTradeOutput[choices[choice][player]]["IDSelfToAssignTD"]);



          for (var j = 0; j < 6; j++) {
            if (newOwnerNeighbors[j] != null) {
              if (map[newOwnerNeighbors[j][0]][newOwnerNeighbors[j][1]]) {
                tuple = [String(newOwnerNeighbors[j][0]), String(newOwnerNeighbors[j][1])];
                hex = tuple.join();
                submarine.push(hex);
              }
            }
          }


          var isSecond = false,
            isThird = false,
            IDOpen1related = Object.values(singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"]);

          for (var subma in IDOpen1related) {
            //check if the new yellow hex is a second to a submarine
            if (IDOpen1related[subma].includes(choice)) {
              singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][choice] = IDOpen1related[subma];

              console.log("running in loop after we found the new hex is a second");
              console.log("we are looking in the children to find the first hex and delete it from IDopen1ToAssignTD");

              for (hex in IDOpen1related[subma]) {
                console.log("IDOpen1related[subma][hex]:");
                console.log(IDOpen1related[subma][hex]);


                if (IDOpen1related[subma][hex] != choice && IDOpen1related[subma][hex] in singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"]) {
                  singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][IDOpen1related[subma][hex]] = IDOpen1related[subma];
                  console.log("this hex found in IDopen1ToAssignTD");
                  console.log("check if we didnt pick the wrong child because of in command: ");
                  console.log("we picked to delete:");
                  console.log(singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"][IDOpen1related[subma][hex]]);

                  delete singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"][IDOpen1related[subma][hex]];
                  console.log("IDopen1ToAssignTD after we delete the first hex that has been found:");
                  console.log(singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"]);
                  break;
                }

              }
              isSecond = true;
              subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 2;
              break;
            }

          }



          //else if the new yellow hex is a first or third to a submarine
          if (!isSecond) {
            var IDOpen2related = Object.values(singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"]);
            for (subma in IDOpen2related) {
              if (IDOpen2related[subma].includes(choice)) {
                singleTradeOutput[choices[choice][player]]["IDopen3ToAssignTD"][choice] = IDOpen2related[subma];

                console.log("running in loop after we found the new hex is a third");
                console.log("we are looking in the children to find the two second hex and delete them from IDopen2ToAssignTD");

                for (hex in IDOpen2related[subma]) {

                  console.log("IDOpen2related[subma][hex]");
                  console.log(IDOpen2related[subma][hex]);

                  if (IDOpen2related[subma][hex] != choice && IDOpen2related[subma][hex] in singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"]) {
                    console.log("this hex found in IDopen2ToAssignTD");
                    console.log("check if we didnt pick the wrong child because of in command: ");
                    console.log("we picked to delete:");
                    console.log(singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][IDOpen2related[subma][hex]]);
                    singleTradeOutput[choices[choice][player]]["IDopen3ToAssignTD"][IDOpen2related[subma][hex]] = IDOpen2related[subma];
                    delete singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][IDOpen2related[subma][hex]];
                    console.log("IDopen2ToAssignTD after we delete the first hex that has been found:");
                    console.log(singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"]);
                  }
                }
                isThird = true;
                subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 3;
                break;
              }
            }
          }
          //new yellow hex is first
          if (!isSecond && !isThird) {
            console.log("new hex is first");
            singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"][choice] = submarine;
            payoff = gameSettings.extraPayoff;
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 1;
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 1;
          } else {//new yellow hex isnt first
            payoff = gameSettings.payoff;
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;
          }

        } else {// if mapvalue=0
          payoff = 0;
          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;
          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 0;
        }


      }
      answers[choices[choice][player]] = {
        payoff: payoff,
        hexs: {},
        roots: {},
        toClose: {}
      };
      answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = mapValue;
    }
  }

  return answers;
}

function patentAnswer(choices, map) {
  console.log('-- patent answer --');
  var answers = {};
  var current_round_patent_owners = {};
  var temporary_new_owners = {};
  var black_hex_owners = {};
  var Thirds = {};
  var xy;

  for (var choice in choices) {
    console.log("running on choice:");
    console.log(choice);
    xy = choice.split(',');
    var mapValue;
    if (xy[0] == -1 || xy[0] == -2) {
      mapValue = -1;
      for (var player in choices[choice]) {
        if (answers[choices[choice][player]] == null) {
          answers[choices[choice][player]] = {
            payoff: xy[0] == -1 ? skipPayoff() : -gameSettings.timeoutPenalty,
            hexs: {},
            roots: {},
            toClose: {}
          }
        } else {//answers[choices[choice][player]]!=null
          if (xy[0] == -1) {
            answers[choices[choice][player]].payoff += skipPayoff();
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = ".";
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = ".";
          } else {
            answers[choices[choice][player]].payoff += -gameSettings.timeoutPenalty;
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = ".";
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = ".";
          }
        }
        black_hex_owners[choices[choice][player]] = 1;
      }
    }
    else {//xy[0] != -1 or -2
      mapValue = map[xy[0]][xy[1]];

      if (mapValue == 1 && !(choice in patent_submarine_owners)) {
        console.log("yellow and no patent");
        tempOwner = Math.floor(Math.random() * (choices[choice].length));
        console.log("tempOwner:");
        console.log(tempOwner);
        temporary_new_owners[choice] = choices[choice][tempOwner];

        for (var player in choices[choice]) {
          //player chose this hex while it was first from his family
          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 1;


          if (player == tempOwner) {
            if (answers[choices[choice][tempOwner]] == null) {
              answers[choices[choice][tempOwner]] = {
                payoff: 0,
                hexs: {},
                roots: {},
                toClose: {}
              }
            }
          }
          else {
            if (answers[choices[choice][player]] == null) {
              answers[choices[choice][player]] = {
                payoff: 0,
                hexs: {},
                roots: {},
                toClose: {}
              }
            }
            black_hex_owners[choices[choice][player]] = 1;
          }

          answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = 'others';
        }
      } else if (mapValue == 1 && (choice in patent_submarine_owners)) {
        console.log("yellow and there is a patent");

        current_round_patent_owners[choice] = patent_submarine_owners[choice];
        black_hex_owners[patent_submarine_owners[choice]] = 1;
        // temporary_new_owners[choice] = patent_submarine_owners[choice];

        tuple = [xy[0], xy[1]];



        IDSelfToAssign[patent_submarine_owners[choice]].push(choice);

        ///Check if the new yellow hexagon is a second or third,in order to update the yellow hexagon arrays for the output file

        console.log("before IDOpen changed:(ID1,ID2,ID3)");
        console.log(IDOpen1ToAssign);
        console.log(IDOpen2ToAssign);
        console.log(IDOpen3ToAssign);

        var isSecond = false;
        var IDOpen1related = Object.values(IDOpen1ToAssign);
        for (submarine in IDOpen1related) {
          //check if the new yellow hex is a second to a submarine
          if (IDOpen1related[submarine].includes(choice)) {
            IDOpen2ToAssign[choice] = IDOpen1related[submarine];

            for (hex in IDOpen1related[submarine]) {
              if (IDOpen1related[submarine][hex] != choice && IDOpen1related[submarine][hex] in IDOpen1ToAssign) {
                IDOpen2ToAssign[IDOpen1related[submarine][hex]] = IDOpen1related[submarine];
                delete IDOpen1ToAssign[IDOpen1related[submarine][hex]];
                break;
              }
            }
            isSecond = true;


            break;
          }
        }

        console.log("isSecond?");
        console.log(isSecond);

        console.log("IDOpen1ToAssign:(if true some hex suppose to be deleted)");
        console.log(IDOpen1ToAssign);


        //else if the new yellow hex is a third to a submarine
        if (!isSecond) {


          var IDOpen2related = Object.values(IDOpen2ToAssign);
          for (submarine in IDOpen2related) {
            if (IDOpen2related[submarine].includes(choice)) {
              IDOpen3ToAssign[choice] = IDOpen2related[submarine];

              for (hex in IDOpen2related[submarine]) {
                if (IDOpen2related[submarine][hex] in rootHexs) {
                  Thirds[rootHexs[IDOpen2related[submarine][hex]]] = 1;
                }
                if (IDOpen2related[submarine][hex] != choice && IDOpen2related[submarine][hex] in IDOpen2ToAssign) {
                  IDOpen3ToAssign[IDOpen2related[submarine][hex]] = IDOpen2related[submarine];
                  delete IDOpen2ToAssign[IDOpen2related[submarine][hex]];
                }
              }
            }

          }
        }
        console.log("IDOpen2ToAssoign:(if false some hex suppose to be deleted)");
        console.log(IDOpen2ToAssign);
        console.log("IDOpen2:(if false some hex suppose to be deleted)");
        console.log(IDOpen2);





        console.log("found some patent owner:");
        console.log(patent_submarine_owners[choice]);

        console.log("player who chose this owned hex:");
        console.log(choices[choice]);

        console.log("patent owner in players array?");
        console.log(choices[choice].includes(patent_submarine_owners[choice]));

        if (choices[choice].includes(patent_submarine_owners[choice])) {
          console.log("patent owner has chosen this hex too");

          for (var player in choices[choice]) {
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = isSecond ? 2 : 3;
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;


            if (choices[choice][player] != patent_submarine_owners[choice]) {
              console.log("this is not my patent :(");

              if (answers[choices[choice][player]] == null) {
                answers[choices[choice][player]] = {
                  payoff: 0,
                  hexs: {},
                  roots: {},
                  toClose: {}
                }

              }
              answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = 'others';
              black_hex_owners[choices[choice][player]] = 1;
            } else if (choices[choice][player] == patent_submarine_owners[choice]) {
              console.log("this is my patent :)");

              //deciding if this is the second or third yellow hex to the owner
              //for inputLine order purpose



              if (answers[choices[choice][player]] == null) {

                answers[choices[choice][player]] = {
                  payoff: gameSettings.payoff,
                  hexs: {},
                  roots: {},
                  toClose: {}
                }

              } else {//answers[choices[choice][player]] != null
                answers[choices[choice][player]].payoff += gameSettings.payoff;
              }
              answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = 1;
            }
          }

        } else {//!patent_submarine_owners[choice] in choices[choice]
          console.log("patent owner has chosen some other hex");
          for (var player in choices[choice]) {

            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = isSecond ? 2 : 3;
            subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;


            if (answers[choices[choice][player]] == null) {

              answers[choices[choice][player]] = {
                payoff: 0,
                hexs: {},
                roots: {},
                toClose: {}
              }

            }
            answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = 'others';
            black_hex_owners[choices[choice][player]] = 1;
          }
          if (answers[patent_submarine_owners[choice]] != null) {
            console.log("patent owner already chose other hex");
            answers[patent_submarine_owners[choice]].payoff +=
              gameSettings.payoff;
            answers[patent_submarine_owners[choice]].hexs[[xy[0], xy[1]]] = 1;
            console.log("answers[patent_submarine_owners[choice]].hexs:");
            console.log(answers[patent_submarine_owners[choice]].hexs);
          } else {
            console.log("patent owner stil didnt chose other hex");
            answers[patent_submarine_owners[choice]] = {
              payoff: gameSettings.payoff,
              hexs: {},
              roots: {},
              toClose: {}
            }

            answers[patent_submarine_owners[choice]].hexs[[xy[0], xy[1]]] = 1;
            console.log("answers[patent_submarine_owners[choice]].hexs:");
            console.log(answers[patent_submarine_owners[choice]].hexs);
          }

        }


      } else if (mapValue == 0) {
        console.log("some black hex selected");
        for (var player in choices[choice]) {

          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 0;
          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;

          if (answers[choices[choice][player]] == null) {
            answers[choices[choice][player]] = {
              payoff: 0,
              hexs: {},
              roots: {},
              toClose: {}
            }
          }
          answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = mapValue;
          black_hex_owners[choices[choice][player]] = 1;
        }
      }

    }
  }// end loop over all the choices in this round

  //all the hexes that are third two a sub
  console.log("Thirds:(after over with one hex in temp_owner)");
  console.log(Thirds);

  // coloring yellow hexes in the grid of players who chose black hex

  console.log("current_round_patent_owners:");
  console.log(current_round_patent_owners);

  console.log("temporary_new_owners:");
  console.log(temporary_new_owners);



  for (var blackHexOwner in black_hex_owners) {
    for (var curPatentOwner in current_round_patent_owners) {
      xy = curPatentOwner.split(',');
      if (blackHexOwner != current_round_patent_owners[curPatentOwner]) {
        answers[blackHexOwner].hexs[[xy[0], xy[1]]] = 'others';
      } else {
        answers[blackHexOwner].hexs[[xy[0], xy[1]]] = 1;
      }
    }

    for (var hex in temporary_new_owners) {
      xy = hex.split(',');
      answers[blackHexOwner].hexs[[xy[0], xy[1]]] = 'others';
    }
  }


  // coloring orange hexes in the grid of players who chose yellow hex

  for (var orangeHex in temporary_new_owners) {
    for (var hex in temporary_new_owners) {
      xy = hex.split(',');
      answers[temporary_new_owners[orangeHex]].hexs[[xy[0], xy[1]]] = 'others';
    }

    for (patent in current_round_patent_owners) {
      xy = patent.split(',');
      if (temporary_new_owners[orangeHex] != current_round_patent_owners[patent]) {
        answers[temporary_new_owners[orangeHex]].hexs[[xy[0], xy[1]]] = 'others'
      }
    }
  }



  for (curPatentOwner in current_round_patent_owners) {
    for (hex in temporary_new_owners) {
      xy = hex.split(',');
      answers[current_round_patent_owners[curPatentOwner]].hexs[[xy[0], xy[1]]] = 'others';
    }
  }


  ////finding ultimate owners after we finish to iterate over choices////

  var neighbors, related;

  for (var hex in temporary_new_owners) {
    if (temporary_new_owners[hex] == null) {
      continue;
    }

    neighbors = [hex];
    xy = hex.split(',');
    related = mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings);


    for (var secondHex in temporary_new_owners) {
      if (temporary_new_owners[secondHex] != null) {
        ab = secondHex.split(',');
        xy = [Number(ab[0]), Number(ab[1])];
        for (var t = 0; t < 6; t++) {
          if (related[t] != null && xy[0] == related[t][0] && xy[1] == related[t][1]) {
            neighbors.push(secondHex);
            break;
          }
        }
      }
    }



    var submarine = [];

    //new_owner:Pick a random number (according to neighbors.length) to represent the new owner

    new_owner = Math.floor(Math.random() * (neighbors.length));
    freshOwner = neighbors[new_owner];
    owner = temporary_new_owners[neighbors[new_owner]];

    answers[owner].payoff += gameSettings.payoff * (neighbors.length - 1) + gameSettings.extraPayoff;
    subjects[io.sockets.connected[owner].subjectID].inputLine["extra"] = 1;
    subjects[io.sockets.connected[owner].subjectID].inputLine["order"] = 1;


    submarine.push(neighbors[new_owner]);


    //capturing all yellow neighbors of new owner for the owner

    xy = neighbors[new_owner].split(',');

    newOwnerNeighbors = mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings);

    console.log("newOwnerNeighbors:");
    console.log(newOwnerNeighbors);




    for (var j = 0; j < 6; j++) {
      if (newOwnerNeighbors[j] != null) {
        if (map[newOwnerNeighbors[j][0]][newOwnerNeighbors[j][1]]) {
          console.log("found some neighbors to add to the owner");
          patent_submarine_owners[[String(newOwnerNeighbors[j][0]), String(newOwnerNeighbors[j][1])]] = owner;

          tuple = [String(newOwnerNeighbors[j][0]), String(newOwnerNeighbors[j][1])];
          hex = tuple.join();
          submarine.push(hex);

        }
      }
    }



    if (neighbors.length == 1) {
      IDOpen1ToAssign[neighbors[0]] = submarine;
    } else if (neighbors.length == 2) {
      for (var k = 0; k < 2; k++) {
        IDOpen2ToAssign[neighbors[k]] = submarine;
      }
    } else {
      for (k = 0; k < 3; k++) {
        IDOpen3ToAssign[neighbors[k]] = submarine;
      }
    }



    for (var j = 0; j < neighbors.length; j++) {
      xy = neighbors[j].split(',');

      //update for csv output file
      if (IDSelfToAssign[owner] == null) {
        IDSelfToAssign[owner] = [neighbors[j]];
      } else {
        IDSelfToAssign[owner].push(neighbors[j]);
      }


      answers[owner].hexs[[xy[0], xy[1]]] = 1;
      // if (j != new_owner) {

      //   answers[temporary_new_owners[neighbors[j]]].payoff += -gameSettings.cost;

      // }
      patent_submarine_owners[neighbors[j]] = owner;
      temporary_new_owners[neighbors[j]] = null;

    }//end of updating the payoffs of new owner and non-owners



    for (hex in neighbors) {
      rootHexs[neighbors[hex]] = freshOwner;
    }


    //update info related to the border around founded hexs and completed submarines which bring a take down of a border

    xy = freshOwner.split(',');
    if (neighbors.length < 3) {
      for (socket in answers) {
        if (socket == owner) {
          answers[socket].roots[freshOwner] = { mine: 1, neighbors: mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings) };
        } else {
          answers[socket].roots[freshOwner] = { mine: 0, neighbors: mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings) };
        }
      }
    }


  }//end of finding ultimate owners after iterate over choices



  console.log("rootHexs:");
  console.log(rootHexs);

  for (socket in answers) {
    for (third in Thirds) {

      xy = third.split(',');
      answers[socket].toClose[third] = mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings);
    }
  }

  for (socket in answers) {
    console.log("answers[socket].roots:");
    for (root in answers[socket].roots) {
      if (answers[socket].roots[root] == null) {
        continue;
      }
      console.log(answers[socket].roots[root]);
    }

    console.log("answers[socket].toClose:");
    console.log(answers[socket].toClose);
  }


  return answers;
}


function noPatentAnswer(choices, map) {
  console.log('nopatent');
  var answers = {};
  var found = {};
  for (var choice in choices) {
    for (var player in choices[choice]) {
      var xy = choice.split(',');
      var payoff;
      var mapValue = 0;
      // squares
      if (xy[0] == -1) {
        payoff = skipPayoff();
        subjects[io.sockets.connected[[choices[choice][player]]].subjectID].inputLine["extra"] = ".";
        subjects[io.sockets.connected[[choices[choice][player]]].subjectID].inputLine["order"] = ".";
      }
      // timeout
      else if (xy[0] == -2) {
        payoff = -gameSettings.timeoutPenalty;
        subjects[io.sockets.connected[[choices[choice][player]]].subjectID].inputLine["extra"] = ".";
        subjects[io.sockets.connected[[choices[choice][player]]].subjectID].inputLine["order"] = ".";
      }
      else {
        mapValue = map[xy[0]][xy[1]];
        submarine = [choice];

        newOwnerNeighbors = mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings);

        if (mapValue) {
          if (IDSelfToAssign[choices[choice][player]] == null) {
            IDSelfToAssign[choices[choice][player]] = [choice];
          } else {
            IDSelfToAssign[choices[choice][player]].push(choice);
          }


          for (var j = 0; j < 6; j++) {
            if (newOwnerNeighbors[j] != null) {
              if (map[newOwnerNeighbors[j][0]][newOwnerNeighbors[j][1]]) {
                tuple = [String(newOwnerNeighbors[j][0]), String(newOwnerNeighbors[j][1])];
                hex = tuple.join();
                submarine.push(hex);

              }
            }
          }

          if (!(choice in IDOpen1ToAssign) && !(choice in IDOpen2ToAssign) && !(choice in IDOpen3ToAssign)) {

            var isSecond = false,
              isThird = false,
              IDOpen1related = Object.values(IDOpen1ToAssign);
            console.log("see what inside IDOpen1related before we check if its a second hex:");
            console.log(IDOpen1related);

            for (var subma in IDOpen1related) {
              console.log("for subma in IDOpen1related --- subma=");
              console.log(subma);
              //check if the new yellow hex is a second to a submarine
              if (IDOpen1related[subma].includes(choice)) {
                IDOpen2ToAssign[choice] = IDOpen1related[subma];

                for (hex in IDOpen1related[subma]) {
                  console.log("for hex in IDOpen1related[subma] --- hex=");
                  console.log(subma);
                  if (IDOpen1related[subma][hex] != choice && IDOpen1related[subma][hex] in IDOpen1ToAssign) {
                    console.log("check if we didnt pick the wrong child because of in command: ");
                    console.log("we picked to delete:");
                    console.log(IDOpen1related[subma][hex]);
                    console.log("IDOpen1ToAssign before delete:");
                    console.log(IDOpen1ToAssign);
                    IDOpen2ToAssign[IDOpen1related[subma][hex]] = IDOpen1related[subma];
                    delete IDOpen1ToAssign[IDOpen1related[subma][hex]];
                    break;
                  }
                }
                isSecond = true;
                break;
              }

            }
            console.log("after done, is it second yellow hex?");
            console.log(isSecond);



            //else if the new yellow hex is a first or third to a submarine
            if (!isSecond) {
              var IDOpen2related = Object.values(IDOpen2ToAssign);
              for (subma in IDOpen2related) {
                if (IDOpen2related[subma].includes(choice)) {
                  IDOpen3ToAssign[choice] = IDOpen2related[subma];
                  for (hex in IDOpen2related[subma]) {
                    if (IDOpen2related[subma][hex] != choice && IDOpen2related[subma][hex] in IDOpen2ToAssign) {
                      console.log("check if we didnt pick the wrong child because of in command: ");
                      console.log("we picked to delete:");
                      console.log(IDOpen2related[subma][hex]);
                      console.log("IDOpen2ToAssign before delete:");
                      console.log(IDOpen2ToAssign);
                      IDOpen3ToAssign[IDOpen2related[subma][hex]] = IDOpen2related[subma];
                      delete IDOpen2ToAssign[IDOpen2related[subma][hex]];
                      // break;
                    }
                  }
                  isThird = true;
                  break;

                }
              }
            }



            if (choice in IDOpen3ToAssign) {
              isThird = true;
            }

            console.log("after done, is it third yellow hex?");
            console.log(isThird);

            console.log("so is this first yellow hex?");
            console.log(!isSecond && !isThird);



            if (!isSecond && !isThird) {// this is the first hex chosen
              IDOpen1ToAssign[choice] = submarine;
              subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 1;
              subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 1;

              if (choices[choice].length == 1) {//only one player chose the first hex
                payoff = gameSettings.extraPayoff;
              } else if (choices[choice].length == 2) {//two players chose the first hex
                payoff = gameSettings.extraPayoff * 0.2;
              } else if (choices[choice].length == 3) {//three players chose the first hex
                payoff = gameSettings.extraPayoff * 0.05;
              } else if (choices[choice].length >= 4) {//four or more players chose the first hex
                payoff = 0;
              }

            } else {// second or third hex chosen
              if (choices[choice].length == 1) {//only one player chose the first hex
                payoff = gameSettings.payoff;
              } else if (choices[choice].length == 2) {//two players chose the first hex
                payoff = gameSettings.payoff * 0.2;
              } else if (choices[choice].length == 3) {//three players chose the first hex
                payoff = gameSettings.payoff * 0.05;
              } else if (choices[choice].length >= 4) {//four or more players chose the first hex
                payoff = 0;
              }

              if (isSecond) {
                subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;
                subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 2;
              } else if (isThird) {
                subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;
                subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 3;
              }


            }
          }
        } else {// map value is 0
          payoff = 0;
          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["extra"] = 0;
          subjects[io.sockets.connected[choices[choice][player]].subjectID].inputLine["order"] = 0;
        }
      }

      answers[choices[choice][player]] = {
        payoff: payoff,
        hexs: {},
        roots: {},
        toClose: {}
      };
      // adds the player hex to the answer
      answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = mapValue;
      // if the chosen hex is 1 adds it to the list of found
      if (mapValue) {
        found[[xy[0], xy[1]]] = mapValue;
      }
    }
  }
  // for all players - adds the hexes in found with 'others' to color is in orange
  for (var player in answers) {
    for (var hex in found) {
      if (!(hex in answers[player].hexs)) {
        answers[player].hexs[hex] = 'others';
      }
    }
  }
  return answers;
}

function tradeSecretsAnswer(choices, map) {
  console.log('tradesecrets');
  var answers = {};
  var xy;
  var mapValue;

  for (var choice in choices) {
    xy = choice.split(',');
    if (xy[0] == -1 || xy[0] == -2) {
      mapValue = -1;
      if (xy[0] == -1) {
        console.log("square selected");
        var pay = skipPayoff();
      } else {
        console.log("time out");
        var pay = -gameSettings.timeoutPenalty;
      }
      for (var player in choices[choice]) {
        answers[choices[choice][player]] = {
          payoff: pay
        }
      }
    } else {//xy[0] != -1
      mapValue = map[xy[0]][xy[1]];

      if (mapValue && !(choice in tradeSecretsHexOwners)) {

        if (yellow_hex_owners_tradesecrets[choice] == null) {
          yellow_hex_owners_tradesecrets[choice] = choices[choice].length;
        } else {
          yellow_hex_owners_tradesecrets[choice] += choices[choice].length;
        }


        for (var player in choices[choice]) {

          //Updates for output file
          submarine = [choice];

          newOwnerNeighbors = mapping.findNeighbors2(Number(xy[0]), Number(xy[1]), gameSettings);


          console.log("singleTradeOutput[choices[choice][player]][IDSelfToAssignTD]:");
          console.log(singleTradeOutput[choices[choice][player]]["IDSelfToAssignTD"]);

          singleTradeOutput[choices[choice][player]]["IDSelfToAssignTD"].push(choice);

          console.log("singleTradeOutput[choices[choice][player]][IDSelfToAssignTD] after push new yellow hex:");
          console.log(singleTradeOutput[choices[choice][player]]["IDSelfToAssignTD"]);



          for (var j = 0; j < 6; j++) {
            if (newOwnerNeighbors[j] != null) {
              if (map[newOwnerNeighbors[j][0]][newOwnerNeighbors[j][1]]) {
                tuple = [String(newOwnerNeighbors[j][0]), String(newOwnerNeighbors[j][1])];
                hex = tuple.join();
                submarine.push(hex);

              }
            }
          }


          var isSecond = false,
            isThird = false,
            IDOpen1related = Object.values(singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"]);

          for (var subma in IDOpen1related) {
            //check if the new yellow hex is a second to a submarine
            if (IDOpen1related[subma].includes(choice)) {
              singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][choice] = IDOpen1related[subma];

              console.log("running in loop after we found the new hex is a second");
              console.log("we are looking in the children to find the first hex and delete it from IDopen1ToAssignTD");

              for (hex in IDOpen1related[subma]) {
                console.log("IDOpen1related[subma][hex]:");
                console.log(IDOpen1related[subma][hex]);


                if (IDOpen1related[subma][hex] != choice && IDOpen1related[subma][hex] in singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"]) {
                  singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][IDOpen1related[subma][hex]] = IDOpen1related[subma];
                  console.log("this hex found in IDopen1ToAssignTD");
                  console.log("check if we didnt pick the wrong child because of in command: ");
                  console.log("we picked to delete:");
                  console.log(singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"][IDOpen1related[subma][hex]]);

                  delete singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"][IDOpen1related[subma][hex]];
                  console.log("IDopen1ToAssignTD after we delete the first hex that has been found:");
                  console.log(singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"]);
                  break;
                }

              }
              isSecond = true;
              break;
            }

          }



          //else if the new yellow hex is a first or third to a submarine
          if (!isSecond) {
            var IDOpen2related = Object.values(singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"]);
            for (subma in IDOpen2related) {
              if (IDOpen2related[subma].includes(choice)) {
                singleTradeOutput[choices[choice][player]]["IDopen3ToAssignTD"][choice] = IDOpen2related[subma];

                console.log("running in loop after we found the new hex is a third");
                console.log("we are looking in the children to find the two second hex and delete them from IDopen2ToAssignTD");

                for (hex in IDOpen2related[subma]) {

                  console.log("IDOpen2related[subma][hex]");
                  console.log(IDOpen2related[subma][hex]);

                  if (IDOpen2related[subma][hex] != choice && IDOpen2related[subma][hex] in singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"]) {
                    console.log("this hex found in IDopen2ToAssignTD");
                    console.log("check if we didnt pick the wrong child because of in command: ");
                    console.log("we picked to delete:");
                    console.log(singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][IDOpen2related[subma][hex]]);
                    singleTradeOutput[choices[choice][player]]["IDopen3ToAssignTD"][IDOpen2related[subma][hex]] = IDOpen2related[subma];
                    delete singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"][IDOpen2related[subma][hex]];
                    console.log("IDopen2ToAssignTD after we delete the first hex that has been found:");
                    console.log(singleTradeOutput[choices[choice][player]]["IDopen2ToAssignTD"]);
                  }
                }
                isThird = true;
                break;

              }
            }
          }

          if (!isSecond && !isThird) {
            console.log("new hex is first");
            singleTradeOutput[choices[choice][player]]["IDopen1ToAssignTD"][choice] = submarine;
          }


          answers[choices[choice][player]] = {
            payoff: gameSettings.payoff / yellow_hex_owners_tradesecrets[choice],
            hexs: {},
            roots: {},
            toClose: {}
          };
          answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = mapValue;
        }

        tradeSecretsHexOwners[choice] = 1;

      } else if (mapValue && (choice in tradeSecretsHexOwners)) {

        for (var player in choices[choice]) {
          answers[choices[choice][player]] = {
            payoff: gameSettings.cost,
            hexs: {},
            roots: {},
            toClose: {}
          }
          answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = 'others';

        }


      } else if (mapValue == 0) {

        for (var player in choices[choice]) {
          answers[choices[choice][player]] = {
            payoff: gameSettings.cost,
            hexs: {},
            roots: {},
            toClose: {}
          }
          answers[choices[choice][player]].hexs[[xy[0], xy[1]]] = mapValue;

        }
      }

    }

  }
  return answers;
}

function resetGame() {
  rootHexs = {};
  subjectsOutputString = {};
  socketsToSubjects = {};
  subjects = {};
  non_singleton_subjects = {};
  singleton_subjects = {};
  disconnected_non_singleton_subjects = {};
  disconnected_singleton_subjects = {};
  ready = 0;
  date = getDateString(),
    path1 = 'public/data/' + date + "M-Singleton",
    path2 = 'public/data/' + date + "M-" + gameSettings.gameMode,
    path = 'public/data/';
  disconnected = false;
  started = false;
  gameSettings.currentGame = 1;
  gameSettings.currentRound = 1;
  gameSettings.currMap = gameSettings.mapNumber;
  map = createMap(gameSettings.mapNumber);
  // map = mapping.mapping(gameSettings);
  Thirds = {},
    IDOpen1 = {},
    IDOpen1ToAssign = {},
    IDOpen2 = {},
    IDOpen2ToAssign = {},
    IDOpen3 = {},
    IDOpen3ToAssign = {},
    IDSelf = {},
    IDSelfToAssign = {},
    IDCounter = 0,
    patent_submarine_owners = {},
    yellow_hex_owners_tradesecrets = {},
    singleTradeOutput = {};
  payoffsCollection = {};
}

function skipPayoff() {
  return gameSettings.skipPay;
}

function updateTimeouts(choices) {
  kick = [];
  for (socket in choices) {
    try {
      s = subjects[choices[socket].subjectID].socket;
      if (choices[socket].x == -2) {
        s.timeouts++;
        if (s.timeouts >= gameSettings.timeoutKick) {
          kick.push(socket);
          s.disconnect();
        }
      }
      else {
        s.timeouts = 0;
      }
    } catch (err) {
      kick.push(socket);
    }
  }
  return kick;
}

function UpdateInputLine(answers) {

  // var checkNeighbors;// neighbors of player's choice at this turn
  // var xCoord, yCoord;//player's choice x,y

  // //player's choice number of yellows/reds
  // var yelCount = 0;
  // var redCount = 0;

  // for (sub in subjects) {
  //   if (subjects[sub].inputLine["index"] != ".") {
  //     xCoord = subjects[sub].inputLine["index"][0];
  //     yCoord = subjects[sub].inputLine["index"][1];

  //     console.log("player's map before checking the neighbors:");
  //     console.log(" ");
  //     var arrText;
  //     var finalHive = maps[subjects[sub].socket.id];
  //     console.log("check wts socket.id and if its same as player id:(soc)");
  //     console.log(subjects[sub].socket.id);

  //     for (k = 0; k < gameSettings.Hl; k++) {
  //       arrText = '';
  //       for (t = 0; t < gameSettings.Hw; t++) {
  //         arrText += finalHive[k][t] + '';
  //         if (finalHive[k][t]) {
  //           yelCount++;
  //         }else if(finalHive[k][t]==0){
  //           redCount++;
  //         }
  //       }
  //       console.log(arrText);
  //     }

  //     checkNeighbors = mapping.findNeighbors2(Number(xCoord), Number(yCoord), gameSettings);

  //     for (var j = 0; j < 6; j++) {
  //       if (checkNeighbors[j] != null) {
  //         if (maps[subjects[sub].socket.id][checkNeighbors[j][0]][checkNeighbors[j][1]] == 1) {
  //           yelCount++;
  //           console.log("found yellow");
  //         } else if (maps[subjects[sub].socket.id][checkNeighbors[j][0]][checkNeighbors[j][1]] == 0) {
  //           redCount++;
  //           console.log("found red");
  //         }
  //       }
  //     }

  //     subjects[sub].inputLine["Yellow"] = yelCount;
  //     subjects[sub].inputLine["Red"] = redCount;

  //     yelCount = 0;
  //     redCount = 0;
  //   }
  // }

  if (Object.keys(singleton_subjects).length > 0) {

    // subjects[s.subjectID].inputLine["Hive"];
    for (var socket in singleton_subjects) {
      if (subjects[socket].inputLine["Hive"] == 0) {
        subjects[socket].inputLine["Payoff"] = Number(answers[singleton_subjects[socket].socket.id].payoff);
      } else if (subjects[socket].inputLine["Hive"] > 0) {
        // subjects[socket].inputLine["Payoff"] = Number(answers[singleton_subjects[socket].socket.id].payoff) - subjects[choices[socket].subjectID].inputLine["lastRoundCost"];
        subjects[socket].inputLine["Payoff"] = Number(answers[singleton_subjects[socket].socket.id].payoff) - subjects[socket].inputLine["lastRoundCost"];
      }


      subjects[socket].inputLine["G"] = gameSettings.currentGame;

      subjects[socket].inputLine["R"] = gameSettings.currentRound;



      subjects[socket].inputLine["IDOpenDiscoveries1"] = [];
      for (open in singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen1TD"]) {
        subjects[socket].inputLine["IDOpenDiscoveries1"].push(open);
      }




      subjects[socket].inputLine["NOpenDiscoveries1"] = Object.keys(singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen1TD"]).length;



      subjects[socket].inputLine["IDOpenDiscoveries2"] = [];
      for (open in singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen2TD"]) {
        subjects[socket].inputLine["IDOpenDiscoveries2"].push(open);
      }


      subjects[socket].inputLine["NOpenDiscoveries2"] = Object.keys(singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen2TD"]).length / 2;



      subjects[socket].inputLine
      ["IDOpenDiscoveries3"] = [];

      for (open in singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen3TD"]) {
        subjects[socket].inputLine["IDOpenDiscoveries3"].push(open);

      }

      subjects[socket].inputLine["NOpenDiscoveries3"] = Object.keys(singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen3TD"]).length / 3;
      console.log("print subjects inputLine to check if we update all the properties for each player,before we send to generateLine");
      console.log("subjects[choices[socket].subjectID].inputLine:");
      console.log(subjects[socket].inputLine);

      if (Object.keys(singleTradeOutput[singleton_subjects[socket].socket.id]["IDSelfTD"]).length != 0) {
        subjects[socket].inputLine["IDSelfOpenDiscoveries"] = singleTradeOutput[singleton_subjects[socket].socket.id]["IDSelfTD"];
      }




      singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen1TD"] = JSON.parse(JSON.stringify(singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen1ToAssignTD"]));
      singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen2TD"] = JSON.parse(JSON.stringify(singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen2ToAssignTD"]));
      singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen3TD"] = JSON.parse(JSON.stringify(singleTradeOutput[singleton_subjects[socket].socket.id]["IDopen3ToAssignTD"]));
      singleTradeOutput[singleton_subjects[socket].socket.id]["IDSelfTD"] = JSON.parse(JSON.stringify(singleTradeOutput[singleton_subjects[socket].socket.id]["IDSelfToAssignTD"]));

    }

  }


  if (gameSettings.gameMode == "Singleton" || gameSettings.gameMode == "Trade Secrets") {
    console.log("game mode is SINGLETON OR TRADESECRETS!");

    console.log("check what choices dict contain before we enter a loop that updates inputLine for each player and uses choices[socket].subjectID and thus one player not considered");
    console.log("choices:");
    console.log(choices);

    for (var socket in answers) {

      if (choices[socket].subjectID in non_singleton_subjects) {


        if (subjects[choices[socket].subjectID].inputLine["Hive"] == 0) {
          subjects[choices[socket].subjectID].inputLine["Payoff"] = Number(answers[socket].payoff);
        } else if (subjects[choices[socket].subjectID].inputLine["Hive"] > 0) {
          subjects[choices[socket].subjectID].inputLine["Payoff"] = Number(answers[socket].payoff) - subjects[choices[socket].subjectID].inputLine["lastRoundCost"];
        }


        // subjects[choices[socket].subjectID].inputLine["Payoff"] = answers[socket].payoff;

        subjects[choices[socket].subjectID].inputLine["G"] = gameSettings.currentGame;

        subjects[choices[socket].subjectID].inputLine["R"] = gameSettings.currentRound;



        subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries1"] = [];
        for (open in singleTradeOutput[socket]["IDopen1TD"]) {
          subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries1"].push(open);
        }




        subjects[choices[socket].subjectID].inputLine["NOpenDiscoveries1"] = Object.keys(singleTradeOutput[socket]["IDopen1TD"]).length;



        subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries2"] = [];
        for (open in singleTradeOutput[socket]["IDopen2TD"]) {
          subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries2"].push(open);
        }


        subjects[choices[socket].subjectID].inputLine["NOpenDiscoveries2"] = Object.keys(singleTradeOutput[socket]["IDopen2TD"]).length / 2;



        subjects[choices[socket].subjectID].inputLine
        ["IDOpenDiscoveries3"] = [];

        for (open in singleTradeOutput[socket]["IDopen3TD"]) {
          subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries3"].push(open);

        }

        subjects[choices[socket].subjectID].inputLine["NOpenDiscoveries3"] = Object.keys(singleTradeOutput[socket]["IDopen3TD"]).length / 3;
        console.log("print subjects inputLine to check if we update all the properties for each player,before we send to generateLine");
        console.log("subjects[choices[socket].subjectID].inputLine:");
        console.log(subjects[choices[socket].subjectID].inputLine);

        if (Object.keys(singleTradeOutput[socket]["IDSelfTD"]).length != 0) {
          subjects[choices[socket].subjectID].inputLine["IDSelfOpenDiscoveries"] = singleTradeOutput[socket]["IDSelfTD"];
        }




        singleTradeOutput[socket]["IDopen1TD"] = JSON.parse(JSON.stringify(singleTradeOutput[socket]["IDopen1ToAssignTD"]));
        singleTradeOutput[socket]["IDopen2TD"] = JSON.parse(JSON.stringify(singleTradeOutput[socket]["IDopen2ToAssignTD"]));
        singleTradeOutput[socket]["IDopen3TD"] = JSON.parse(JSON.stringify(singleTradeOutput[socket]["IDopen3ToAssignTD"]));
        singleTradeOutput[socket]["IDSelfTD"] = JSON.parse(JSON.stringify(singleTradeOutput[socket]["IDSelfToAssignTD"]));

      }





    }

  } else {// noPatent ot Patent mode
    console.log("game mode is Patent or Nopatent");
    for (var socket in answers) {

      if (choices[socket].subjectID in non_singleton_subjects) {

        if (subjects[choices[socket].subjectID].inputLine["Hive"] == 0) {
          subjects[choices[socket].subjectID].inputLine["Payoff"] = Number(answers[socket].payoff);
        } else if (subjects[choices[socket].subjectID].inputLine["Hive"] > 0) {
          subjects[choices[socket].subjectID].inputLine["Payoff"] = Number(answers[socket].payoff) - subjects[choices[socket].subjectID].inputLine["lastRoundCost"];
        }

        // subjects[choices[socket].subjectID].inputLine["Payoff"] = answers[socket].payoff;


        subjects[choices[socket].subjectID].inputLine["G"] = gameSettings.currentGame;

        subjects[choices[socket].subjectID].inputLine["R"] = gameSettings.currentRound;



        subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries1"] = [];
        for (open in IDOpen1) {
          subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries1"].push(open);
        }

        subjects[choices[socket].subjectID].inputLine["NOpenDiscoveries1"] = Object.keys(IDOpen1).length;



        subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries2"] = [];
        for (open in IDOpen2) {
          subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries2"].push(open);
        }


        subjects[choices[socket].subjectID].inputLine["NOpenDiscoveries2"] = Object.keys(IDOpen2).length / 2;



        subjects[choices[socket].subjectID].inputLine
        ["IDOpenDiscoveries3"] = [];

        for (open in IDOpen3) {
          subjects[choices[socket].subjectID].inputLine["IDOpenDiscoveries3"].push(open);


        }

        subjects[choices[socket].subjectID].inputLine["NOpenDiscoveries3"] = Object.keys(IDOpen3).length / 3;

        if ((socket in IDSelf) && Object.keys(IDSelf[socket]).length != 0) {
          subjects[choices[socket].subjectID].inputLine["IDSelfOpenDiscoveries"] = IDSelf[socket];
        }


        IDCounter++;
      }


      // for (var self in IDSelf) {
      //   if (Object.keys(IDSelf[self]).length != 0) {
      //     subjects[choices[self].subjectID].inputLine["IDSelfOpenDiscoveries"] = IDSelf[self];
      //   }
      // }


      if (IDCounter == Object.keys(non_singleton_subjects).length) {

        IDOpen1 = JSON.parse(JSON.stringify(IDOpen1ToAssign));
        IDOpen2 = JSON.parse(JSON.stringify(IDOpen2ToAssign));
        IDOpen3 = JSON.parse(JSON.stringify(IDOpen3ToAssign));
        IDSelf = JSON.parse(JSON.stringify(IDSelfToAssign));
        IDCounter = 0;
      }


    }


  }
  generateLine();
}

function generateLine() {

  for (var ID in subjects) {

    var
      identication = subjects[ID].inputLine["ID"],
      age = subjects[ID].inputLine["age"],
      gender = subjects[ID].inputLine["gender"],
      curGame = subjects[ID].inputLine["G"],

      curRound = subjects[ID].inputLine["R"],
      gMode,
      nOpD1 = subjects[ID].inputLine["NOpenDiscoveries1"],
      nOpD2 = subjects[ID].inputLine["NOpenDiscoveries2"],
      nOpD3 = subjects[ID].inputLine["NOpenDiscoveries3"],
      idOpD1 = subjects[ID].inputLine["IDOpenDiscoveries1"],
      idOpD1array = [],
      idOpD1string,
      idOpD2 = subjects[ID].inputLine["IDOpenDiscoveries2"],
      idOpD2array = [],
      idOpD2string,
      idOpD3 = subjects[ID].inputLine["IDOpenDiscoveries3"],
      idOpD3array = [],
      idOpD3string,
      idSeOp = subjects[ID].inputLine["IDSelfOpenDiscoveries"],
      idSeOparray = [],
      idSeOpstring,
      hv = subjects[ID].inputLine["Hive"],
      hx = subjects[ID].inputLine["Hexagon"],
      yels = subjects[ID].inputLine["Yellow"],
      reds = subjects[ID].inputLine["Red"],
      pff = subjects[ID].inputLine["Payoff"],
      ord = subjects[ID].inputLine["order"],
      ext = subjects[ID].inputLine["extra"];
    cst = subjects[ID].inputLine["lastRoundCost"];



    if (ID in singleton_subjects) {
      gMode = "Singleton";
    } else {
      gMode = gameSettings.gameMode;
    }


    if (nOpD1 == 0) {
      idOpD1string = ".";
    } else {
      for (var idop in idOpD1) {


        xy = idOpD1[idop].split(',');
        // tuple = "(" + idOpD1[idop] + ")";
        tuple = '(' + xy[0] + '-' + xy[1] + ')';

        idOpD1array.push(tuple);
      }
      idOpD1string = idOpD1array.toString();
      // idOpD1string = idOpD1string.replace(",", " ");
      idOpD1string = idOpD1string.replace(/,/g, " ");
    }



    if (nOpD2 == 0) {
      idOpD2string = ".";
    } else {
      for (idop in idOpD2) {
        xy = idOpD2[idop].split(',');
        tuple = "(" + xy[0] + "-" + xy[1] + ")";

        idOpD2array.push(tuple);
      }
      idOpD2string = idOpD2array.toString();
      // idOpD2string = idOpD2string.replace(",", " ");
      idOpD2string = idOpD2string.replace(/,/g, " ");

    }


    if (nOpD3 == 0) {
      idOpD3string = ".";
    } else {
      for (idop in idOpD3) {
        xy = idOpD3[idop].split(',');
        tuple = "(" + xy[0] + "-" + xy[1] + ")";

        idOpD3array.push(tuple);
      }
      idOpD3string = idOpD3array.toString();
      // idOpD3string = idOpD3string.replace(",", " ");
      idOpD3string = idOpD3string.replace(/,/g, " ");

    }

    if (Object.keys(idSeOp).length != 0) {
      for (idop in idSeOp) {
        xy = idSeOp[idop].split(',');
        tuple = "(" + xy[0] + "-" + xy[1] + ")";

        idSeOparray.push(tuple);
      }
      idSeOpstring = idSeOparray.toString();
      // idSeOpstring = idSeOpstring.replace(",", " ");
      idSeOpstring = idSeOpstring.replace(/,/g, " ");
    } else {
      idSeOpstring = ".";
    }









    subjectsOutputString[ID] += [

      gameSettings.Hl, gameSettings.Hw, gameSettings.CHrate, gameSettings.minCost, gameSettings.maxCost, cst, gameSettings.payoff, gameSettings.extraPayoff, gMode, gameSettings.Ngames, gameSettings.Nrounds, gameSettings.Nplayers, identication, age, gender, curGame, gameSettings.currMap, curRound, nOpD1, nOpD2, nOpD3, idOpD1string, idOpD2string, idOpD3string, idSeOpstring, hv, hx, ord, ext, yels, reds, pff
    ] + "\n";

    if (subjects[choices[socket].subjectID].inputLine["R"] == gameSettings.Nrounds) {
      subjects[ID].inputLine["IDSelfOpenDiscoveries"] = [];
      // if (subjects[choices[socket].subjectID].inputLine["G"] == gameSettings.Ngames) {
      //   subjectsOutputString[ID] += "\n" + ["Date",dateToFile]+ "\n" + "\n" + [

      //     "Show up", gameSettings.showUp
      //   ] + "\n" + ["Reward", reward] + "\n" + ["Total", gameSettings.showUp + reward];
      // }
    }


    console.log("subjectsOutputString[ID]:");
    console.log(subjectsOutputString[ID]);

  }
}
function getDateString() {
  var day = new Date().getDate();
  var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();

  var houres = (new Date().getHours() + 2) % 24;
  var minutes = new Date().getMinutes();

  strToReturn = "D" + day + "-" + month + "-" + year + "H" + houres + "-" + minutes;
  return strToReturn;
}





function writeToFile() {
  //creates folder for current time
  // var date = new Date().toJSON().slice(0, 19).replace('T', ' ');
  // var path1 = 'public/data/' + date + '-Singleton-map';
  // var path2 = 'public/data/' + date + '-' + gameSettings.gameMode;
  // var path = 'public/data/';

  if (Object.keys(singleton_subjects).length > 0) {
    console.log(path1);
    if (!fs.existsSync(path1)) {
      fs.mkdirSync(path1);
    }
  }

  console.log(path2);
  if (!fs.existsSync(path2)) {
    fs.mkdirSync(path2);
  }

  // console.log(path);
  // if (!fs.existsSync(path)) {
  //   fs.mkdirSync(path);
  // }



  for (ID in subjectsOutputString) {
    if (!(ID in disconnected_non_singleton_subjects) && !(ID in disconnected_singleton_subjects)) {
      if (ID in non_singleton_subjects) {
        name = path2 + '/' + ID + '.csv';
        var ws = fs.createWriteStream(name);
        ws.write(subjectsOutputString[ID]);
      } else if (ID in singleton_subjects) {
        name = path1 + '/' + ID + '.csv';
        var ws = fs.createWriteStream(name);
        ws.write(subjectsOutputString[ID]);
      }
    }

  }

}