function validateForm() {
    if (validateGames() && validateRounds() && validatePlayers() && validateCH() && validateHiveWidth() && validateHiveLength() && validateMaxCost() &&validateMinCost()  && validatePayoff() && validateExtraPayoff() && validateExchangeRate() && validateShowUp()) {
        $("#save").attr("disabled", false);
    }
    else {
        $("#save").attr("disabled", true);
    }
}


function validateGames() {
    if ($("#Ngames").val() < 1 || !$.isNumeric($("#Ngames").val()) || $("#Ngames").val().length < 1) {
        $("#Ngames").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#Ngames").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}


function validateRounds() {
    if ($("#Nrounds").val() < 1 || !$.isNumeric($("#Nrounds").val()) || $("#Nrounds").val().length < 1) {
        $("#Nrounds").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#Nrounds").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

function validatePlayers() {
    if ($("#Nplayers").val() < 1 || !$.isNumeric($("#Nplayers").val()) || $("#Nplayers").val().length < 1) {
        $("#Nplayers").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#Nplayers").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

function validateSquares() {
    if (!$.isNumeric($("#Nsquares").val()) || $("#Nsquares").val().length < 1) {
        $("#Nsquares").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#Nsquares").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

function validateHiveLength() {
    if ($("#Hl").val() < 1 || !$.isNumeric($("#Hl").val()) || $("#Hl").val().length < 1) {
        $("#Hl").parent().siblings("td:nth-child(3)").show();

        return false;
    }
    else {
        $("#Hl").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

function validateHiveWidth() {
    if ($("#Hw").val() < 1 || !$.isNumeric($("#Hw").val()) || $("#Hw").val().length < 1) {
        $("#Hw").parent().siblings("td:nth-child(3)").show();

        return false;
    }
    else {
        $("#Hw").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}


function validateCH() {
    if ($("#chRate").val() < 0 || $("#chRate").val() > 1 || !$.isNumeric($("#chRate").val()) || $("#chRate").val().length < 1) {
        $("#chRate").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#chRate").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}



function validateMinCost() {
    if ($("#minCost").val() < 0 || !$.isNumeric($("#minCost").val()) || $("#minCost").val().length < 1) {
        $("#minCost").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#minCost").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

function validateMaxCost() {
    if ($("#maxCost").val() < 0 || !$.isNumeric($("#maxCost").val()) || $("#maxCost").val().length < 1) {
        $("#maxCost").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#maxCost").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}


function validatePayoff() {
    if ($("#payoff").val() < 0 || !$.isNumeric($("#payoff").val()) || $("#payoff").val().length < 1) {
        $("#payoff").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#payoff").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

function validateExtraPayoff() {
    if ($("#extraPayoff").val() < 0 || !$.isNumeric($("#extraPayoff").val()) || $("#extraPayoff").val().length < 1) {
        $("#extraPayoff").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#extraPayoff").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}


function validateExchangeRate() {
    if ($("#exchangeRate").val() < 0 || !$.isNumeric($("#exchangeRate").val()) || $("#exchangeRate").val().length < 1) {
        $("#exchangeRate").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#exchangeRate").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}


function validateShowUp() {
    if ($("#showUp").val() < 0 || !$.isNumeric($("#showUp").val()) || $("#showUp").val().length < 1) {
        $("#showUp").parent().siblings("td:nth-child(3)").show();
        return false;
    }
    else {
        $("#showUp").parent().siblings("td:nth-child(3)").hide();
        return true;
    }
}

$('#save').click(function () {
    $.ajax({
        type: "POST",
        url: '/admin',
        data: {
            Ngames: $('#Ngames').val(),
            Nrounds: $('#Nrounds').val(),
            Nplayers: $('#Nplayers').val(),
            Nsquares: $('#Nsquares').val(),
            Hl: $('#Hl').val(),
            Hw: $('#Hw').val(),
            CHrate: $('#chRate').val(),
            minCost: $('#minCost').val(),
            maxCost: $('#maxCost').val(),
            payoff: $('#payoff').val(),
            extraPayoff: $('#extraPayoff').val(),
            currency: $('#currency').val(),
            exchangeRate: $('#exchangeRate').val(),
            showUp: $('#showUp').val(),
            timeoutTime: $('#timeoutTime').val(),
            timeoutPenalty: $('#timeoutPenalty').val(),
            timeoutKick: $('#timeoutKick').val(),
            messageTime: $('#messageTime').val(),
            mode: $('#mode').val(),
            mapNumber: $('#mapNumber').val(),
        },
        success: (data) => {
            location.reload();
        }
    });

});

