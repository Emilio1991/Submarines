
Hl = 30
Hw = 70
Nsquares = 0;



$.get("/settings", function (data) {
    Hl = data.Hl;
    Hw = data.Hw;
});

/////////////////////////GRID////////////////////////////

$.fn.hexGridWidget = function (radius, columns, rows, cssClass) {
    'use strict';
    var createSVG = function (tag) {
        return $(document.createElementNS('http://www.w3.org/2000/svg', tag || 'svg'));
    };
    return $(this).each(function () {
        var element = $(this),
            hexClick = function () {
                var hex = $(this);
                element.trigger($.Event('hexclick', hex.data()));

            },
            height = Math.sqrt(3) / 2 * radius,
            svgParent = createSVG('svg').attr('tabindex', 1).appendTo(element).css({
                width: (1.5 * columns + 0.5) * radius,
                height: (2 * rows + 1) * height
            }),
            column, row, center,
            toPoint = function (dx, dy) {
                return Math.round(dx + center.x) + ',' + Math.round(dy + center.y);
            };
        for (row = 0; row < rows; row++) {
            for (column = 0; column < columns; column++) {
                center = { x: Math.round((1 + 1.5 * column) * radius), y: Math.round(height * (1 + row * 2 + (column % 2))) };
                createSVG('polygon').attr({
                    points: [
                        toPoint(-1 * radius / 2, -1 * height),
                        toPoint(radius / 2, -1 * height),
                        toPoint(radius, 0),
                        toPoint(radius / 2, height),
                        toPoint(-1 * radius / 2, height),
                        toPoint(-1 * radius, 0)
                    ].join(' '),
                    'class': cssClass,
                    tabindex: 1
                })
                    .appendTo(svgParent).data({ center: center, row: row, column: column }).on('click', hexClick).attr({ 'hex-row': row, 'hex-column': column });
            }
        }
    });
};





var rebuild = function (Hl, Hw) {
    var
        radius = 20,
        columns = Hw,
        rows = Hl,
        cssClass = 'hexfield';

    $('#container').empty().hexGridWidget(radius, columns, rows, cssClass);
    $('#container .hexfield').click(function () {
        var setColorTo = 0;
        if (this.classList.contains('clickedCorrect')) {
            this.classList.remove('clickedCorrect');
            console.log("removed yellow color");
            this.classList.add('clickedWrong');
            console.log("added black color");
        } else if (this.classList.contains('clickedWrong')) {
            this.classList.remove('clickedWrong');
            console.log("removed black color");
            this.classList.add('clickedCorrect');
            console.log("added yellow color");
            setColorTo = 1;
        }

        $.ajax({
            type: "POST",
            url: '/map',
            data: {
                hexRow: this.getAttribute('hex-row'),
                hexCol: this.getAttribute('hex-column'),
                newColor: setColorTo
            },
            success: (data) => {
                location.reload();
            }
        });
        //     $("#cover").show();
        //     $("#container").css('filter', 'blur(2px)');
        //     $("#message").text("Waiting for other players to make their choice...");
        //     $("#message").css('background', 'rgba(100,100,100,0.7)');
        //     $("#message").fadeIn(500);
        //     console.log('[' + this.getAttribute('hex-row') + ',' + this.getAttribute('hex-column') + ']');
        //     socket.emit('play', { subjectID: subID, x: this.getAttribute('hex-row'), y: this.getAttribute('hex-column') });


    });

};

rebuild(Hl, Hw);

///////////////////////////////////////////////////////////////////

$("#game").delay(400).fadeIn(500);







