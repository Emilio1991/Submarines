var socket = io.connect();

textField1 = document.getElementById('instructionsManual1');
textFieldSingleton = document.getElementById('instructionsManualSingleton');
textFieldTradeSecrets = document.getElementById('instructionsManualTrade');
textFieldnoPatent = document.getElementById('instructionsManualnoPatent');
textFieldPatent = document.getElementById('instructionsManualPatent');


string1=`    <h1>ברוכים הבאים למשחק.</h1>
<p>
    במשחק זה מטרתכם להשיג את מספר הנקודות הרב ביותר.<br />
בכל תור עומדות בפניך 2 אפשרויות: לבחור באחת המשבצות בנהר (the river) או באחד המשושים באזור החפירות (the digging area).
</p>
<p>
<b>    הנהר:</b><br />
לכל אחד מהשחקנים יש חלק אחר בנהר בו הוא יכול לדוג דגים. הניקוד יכול להיות חיובי במידה ונמצא דג או שלילי במידה ולא.<br />
מכיוון שהנהר זורם, לחיצה באותה משבצת מספר פעמים תניב רווחים שונים. הרווחים אינם תלויים בפעולות השחקנים האחרים.
</p>
<p>
<b>אזור החפירות:</b><br/>
כאן עליכם לנסות למצוא בארות נפט. כל באר נפט מכילה 3 משושים ונראית באופן הבא: 
</p>
<img src="images/submarine1.png" height="60px"/>
<span style="margin: 0 10px 0 10px;"> או </span>
<img src="images/submarine2.png" height="60px" />
<br/><br/>
{{#ifNotEqueal settings.gameMode 'Singleton'}}
    <p>שטח החיפוש משותף לכולם, ולכן פעולות השחקנים האחרים משפיעות על הרווחים שלכם.
{{/ifNotEqueal}}
</p>
<img src="images/gameboard.jpg" style="float: left;"/>
<button type="button" class="btn btn-primary" id="continue-general">המשך</button>`;


stringSingleton=`       <p>
במידה ולא מצאתם, תשלמו את עלות החיפוש.
<br /> המשושה יצבע בשחור אצלכם על המסך (ולא במסכים של השחקנים האחרים) ולא תוכלו ללחוץ שוב על אותו משושה.
</p>
<p>
במידה ומצאתם, אתם תקבלו את רווחי הנפט באותו משושה,
<br/> המשושה יצבע בצהוב רק במסך שלכם, ולא יצבע כלל במסכים האחרים.
</p>
<p>
שימו לב כי גודל כל באר נפט היא 3 משושים.
<br/> בכל תור תוכלו לבדוק משושה אחד בלבד. החיפוש מוגבל לאזור החיפוש בלבד,
<br/> ולכן על שפת אזור החיפוש (במסגרת ה"כוורת") יתכנו בארות נפט שחלקם מחוץ לשטח המותר בחיפוש.
</p>
<p>
לאחר שמשושה נצבע בצבע כלשהו (שחור\ צהוב) לא ניתן לבחור בו שוב.
</p>
<img src="images/inst1.jpg" />
<img src="images/inst2.jpg" />`;


stringTradeSecrets=`<p>
במידה ולא מצאתם, תשלמו את עלות החיפוש.
<br /> המשושה יצבע בשחור אצלכם על המסך (ולא במסכים של השחקנים האחרים) ולא תוכלו ללחוץ שוב על אותו משושה.
</p>
<p>
במידה ומצאתם, אתם תקבלו את רווחי הנפט באותו משושה,
<br/> המשושה יצבע בצהוב רק במסך שלכם, ולא יצבע כלל במסכים האחרים.
</p>
<p>
ככל שיותר שחקנים בחרו בתורות הקודמים באותו משושה עם נפט,
<br/> כך הרווח מאותו משושה בתורות הבאים יורד.
</p>
<p>
שימו לב כי גודל כל באר נפט היא 3 משושים.
<br/> בכל תור תוכלו לבדוק משושה אחד בלבד. החיפוש מוגבל לאזור החיפוש בלבד,
<br/> ולכן על שפת אזור החיפוש (במסגרת ה"כוורת") יתכנו בארות נפט שחלקם מחוץ לשטח המותר בחיפוש.
</p>
<p>
לאחר שמשושה נצבע בצבע כלשהו (שחור\ צהוב) לא ניתן לבחור בו שוב.
</p>
<img src="images/inst1.jpg" />
<img src="images/inst2.jpg" />`;


stringnoPatent=` <p>
במידה ולא מצאתם, תשלמו את עלות החיפוש.
<br /> המשושה יצבע בשחור אצלכם על המסך (ולא במסכים של השחקנים האחרים) ולא תוכלו ללחוץ שוב על אותו משושה.
</p>
<p>
במידה ומצאתם, אתם תקבלו את רווחי הנפט באותו משושה,
<br/> המשושה יצבע בצהוב אצלכם, ובאדום במסך של השחקנים האחרים.
<br/> במידה ושחקן אחר מצא נפט, המשושה שבחר יסומן באדום על המסך שלכם.
</p>
<p>
אם שני שחקנים או יותר בוחרים את אותו משושה המכיל נפט בו זמנית,
<br/> הרווח ממנו יתחלק באופן שווה בין השחקנים שבחרו בו.
</p>
<p>
שימו לב כי גודל כל באר נפט היא 3 משושים.
<br/> בכל תור תוכלו לבדוק משושה אחד בלבד. החיפוש מוגבל לאזור החיפוש בלבד,
<br/> ולכן על שפת אזור החיפוש (במסגרת ה"כוורת") יתכנו בארות נפט שחלקם מחוץ לשטח המותר בחיפוש.
</p>
<p>
לאחר שמשושה נצבע בצבע כלשהו (שחור\ צהוב\אדום) לא ניתן לבחור בו שוב.
</p>
<img src="images/inst1.jpg" />
<img src="images/inst3.jpg" />

`
stringPatent=`<p>
במידה ולא מצאתם, תשלמו את עלות החיפוש.
<br /> המשושה יצבע בשחור אצלכם על המסך (ולא במסכים של השחקנים האחרים) ולא תוכלו ללחוץ שוב על אותו משושה.
</p>
<p>
במידה ומצאתם, אתם תקבלו את רווחי הנפט באותו משושה,
<br/> המשושה יצבע בצהוב אצלכם, ובאדום במסך של השחקנים האחרים.
<br/> משושה צהוב, מעניק לכם רישיון בלעדי לחפש נפט במשושים הסמוכים לו.
<br/> משושה אדום המופיע על המסך שלכם, משמעותו היא כי שחקן אחד זכה בבלעדיות על חיפוש נפט סביב משושה שמצא.
</p>
<p>
אם שני שחקנים או יותר בוחרים את אותו משושה בו זמנית, כולם משלמים את עלות החיפוש.
<br/> הרווח וכן הבלעדיות על הרווח מהמשושים הסמוכים יגיעו לאחד מבין שחקנים אלו שיבחר באקראי.
</p>
<p>
שימו לב כי גודל כל באר נפט היא 3 משושים.
<br/> בכל תור תוכלו לבדוק משושה אחד בלבד. החיפוש מוגבל לאזור החיפוש בלבד,
<br/> ולכן על שפת אזור החיפוש (במסגרת ה"כוורת") יתכנו בארות נפט שחלקם מחוץ לשטח המותר בחיפוש.
</p>
<p>
לאחר שמשושה נצבע בצבע כלשהו (שחור\ צהוב\אדום) לא ניתן לבחור בו שוב.
</p>
<img src="images/inst1.jpg" />
<img src="images/inst4.jpg" />`;

textField1.value=string1;

textFieldSingleton.value=stringSingleton;
textFieldTradeSecrets.value=stringTradeSecrets;
textFieldnoPatent.value= stringnoPatent;
textFieldPatent.value=stringPatent;




$('#saveInst').click(function () {
    $.ajax({
        type: "POST",
        url: '/instructions',
        data: {
            general: textField1.value,
            Singleton: textFieldSingleton.value,
            TD: textFieldTradeSecrets.value,
            NP:textFieldnoPatent.value,
            Patent: textFieldPatent.value,
        },
        success: (data) => {
            location.reload();
        }
    });

});
