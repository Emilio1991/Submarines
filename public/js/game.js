// make connection
var socket = io.connect();
///////////////////////////////////////////////////
var isSingleton=false;


new_id = Math.floor(Math.random() * 314483687);
new_age = Math.floor(Math.random() * 100);
new_gender = (Math.random());

function generateCostForNewRound() {
    let costsArray = [5, 10, 15, 20, 25, 30, 35];
    let pickedCost = Math.round(Math.random() * 6);
    currentRoundCost = costsArray[pickedCost];
    costs.push(costsArray[pickedCost]);
  }

////////////DEVELOPMENT QA MODE- TURN NOTES OFF TO GET THE MODE////////////


// $("#subjectID").val(new_id);
// $("#age").val(new_age);

// if (new_gender >= 0.5) {
//     $("#genderM").prop("checked", true);

// } else {
//     $("#genderF").prop("checked", true);
// }

/////////////////////////////////////////////////////


// $("#answer1").prop("checked", true);
// $("#answer2").prop("checked", true);
// $("#answer3").prop("checked", true);
// $("#answer4").prop("checked", true);




/////////////////////////////////////////////////////
var gameSettings;
var ready = false;
var start = false;
var costs=[];
var currentRoundCost;
var playedSkip = true;
var subID;
Hl = 0
Hw = 0
Nsquares = 0;
var timer, timeout;
a = 0;
newGame = false;
var messageTime = 1000;
settings = 0;
var global_var_grid_width = 0;
examObj = {
    singleton: {
        questions: [
            "1. מה משמעותו של משושה שחור על המסך שלי?",
            "2. מתי עלי לשלם את עלות החיפוש?",
            "3. כמה משושים יש בכל מכרה זהב",
            "4. מה משמעותו של משושה צהוב על המסך שלי?"
        ],
        answers: [
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב.",
            "אף שחקן לא בחר במשושה בתור הקודם.",
            "זהו משושה שבדקתי ומצאתי כי אין בו זהב.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.",
            "בכל פעם שאני בוחר לחפש זהב.",
            "בכל פעם שאני לא מוצא זהב",
            "בכל פעם שאני מוצא זהב",
            "בכל תור במשחק.",
            "2",
            "4",
            "5",
            "3, ובקצוות שטח החיפוש יתכנו מכרות זהב עם 1 או 2 משושים.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.",
            "זהו משושה שנבדק ונמצא כי אין בו זהב.",
            "זהו משושה שאפשר לבחור בו שוב.",
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב.",
        ]
    },
    trade: {
        questions: [
            "1. מה משמעותו של משושה שחור על המסך שלי?",
            "2. אם אלחץ פעמיים על אותו ריבוע בנהר מה יקרה?",
            "3. אם אלחץ על משושה עם זהב לאחר שכבר שחקן אחר לחץ עליו, מה יקרה?",
            "4. מה משמעותו של משושה צהוב על המסך שלי?"
        ],
        answers: [
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב.",
            "אף שחקן לא בחר במשושה בתור הקודם.",
            "זהו משושה שבדקתי ומצאתי כי אין בו זהב.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.",
            "בכל פעם יתכן ואקבל ניקוד שונה.",
            "בכל פעם אקבל אותו ניקוד.",
            "אי אפשר ללחוץ פעמיים על אותו ריבוע.",
            "אקבל בוודאות 0.",
            "לא אקבל את רווחי המשושה, אבל לא אשלם את עלות החיפוש.",
            "לא אקבל את רווחי המשושה וגם אשלם עלות החיפוש.",
            "אקבל את מלוא רווחי המשושה.",
            "אקבל פחות מהרווחים של המשושה, ככל שיותר שחקנים בחרו אותו בעבר.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.",
            "זהו משושה שנבדק ונמצא כי אין בו זהב.",
            "זהו משושה שאפשר לבחור בו שוב.",
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב.",
        ]
    },
    patent: {
        questions: [
            "1. מה משמעותו של משושה אדום על המסך שלי?",
            "2. מתי עלי לשלם את עלות החיפוש?",
            "3. אם שני שחקנים לוחצים על אותו משושה בו זמנית, מה יקרה?",
            "4. מה משמעותו של משושה צהוב על המסך שלי?"
        ],
        answers: [
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב,אוכל להרוויח אם אמצא משושה נוסף ששייך לאותו מכרה זהב.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב. <br/><p class='newLineAnswer'>אוכל להרוויח אם אמצא משושה נוסף ששייך לאותו מכרה זהב.</p>",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.<br/><p class='newLineAnswer'>לא אוכל להרוויח אם אמצא משושה נוסף ששייך לאותו מכרה זהב, כי רק השחקן שבחר במשושה האדום יכול להרוויח מהמשושים הסמוכים.</p>",
            "זהו משושה שנבדק ונמצא כי אין בו זהב.",
            "בכל פעם שאני בוחר לחפש זהב.",
            "בכל פעם שאני לא מוצא זהב.",
            "בכל פעם שאני מוצא זהב.",
            "בכל תור במשחק.",
            "אם יש במשושה זהב, הרווח מתחלק בין שניהם.",
            "אם אין במשושה זהב, רק אחד ישלם את עלות החיפוש.",
            "אם יש במשושה זהב, הראשון שלחץ עליו יקבל את הרווח.",
            "אם יש במשושה זהב,המחשב יבחר אחד באופן אקראי שיקבל את הרווח, והשאר לא יקבלו דבר. אם אין במשושה זהב כולם משלמים עלות חיפוש.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב. אוכל להרוויח אם אמצא משושה נוסף ששייך לאותו מכרה זהב.",
            "זהו משושה שנבדק ונמצא כי אין בו זהב.",
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב, כל שחקן יכול להרוויח מהמשושים הסמוכים למשושה הצהוב",
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב, רק אני יכול להרוויח מהמשושים הסמוכים.",
        ]
    },
    noPatent: {
        questions: [
            "1. מה משמעותו של משושה אדום על המסך שלי?",
            "2. מתי עלי לשלם את עלות החיפוש?",
            "3. אם שני שחקנים לוחצים על אותו משושה בו זמנית, מה יקרה?",
            "4. מה משמעותו של משושה צהוב על המסך שלי?"
        ],
        answers: [
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב.",
            "אף שחקן לא בחר במשושה בתור הקודם.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.",
            "זהו משושה שנבדק ונמצא כי אין בו זהב.",
            "בכל פעם שאני בוחר לחפש זהב.",
            "בכל פעם שאני לא מוצא זהב.",
            "בכל פעם שאני מוצא זהב.",
            "בכל תור במשחק.",
            "אם יש במשושה זהב, המחשב יבחר אחד באופן אקראי שיקבל את הרווח.",
            "אם אין במשושה זהב,רק אחד ישלם את עלות החיפוש.",
            "אם יש במשושה זהב, הראשון שלחץ עליו יקבל את הרווח.",
            "אם יש במשושה זהב, כל שחקן יקבל פחות ממה שהיה מקבל אילו לחץ לבד.",
            "שחקן אחר בחר במשושה באחד התורות הקודמים ומצא בו זהב.",
            "זהו משושה שנבדק ונמצא כי אין בו זהב.",
            "זהו משושה שאפשר לבחור בו שוב.",
            "בחרתי את המשושה באחד התורות הקודמים ומצאתי בו זהב.",
        ]
    }
}

// $("#inst1").innerHTML("2");
/////////////////////////GRID////////////////////////////


$.fn.hexGridWidget = function (radius, columns, rows, cssClass) {
    'use strict';
    var createSVG = function (tag) {
        return $(document.createElementNS('http://www.w3.org/2000/svg', tag || 'svg'));
    };
    return $(this).each(function () {
        var element = $(this),
            hexClick = function () {
                var hex = $(this);
                element.trigger($.Event('hexclick', hex.data()));

            },
            height = Math.sqrt(3) / 2 * radius,
            svgParent = createSVG('svg').attr('tabindex', 1).appendTo(element).css({
                width: (1.5 * columns + 0.5) * radius,
                height: (2 * rows + 1) * height
            }),

            column, row, center,
            toPoint = function (dx, dy) {
                return Math.round(dx + center.x) + ',' + Math.round(dy + center.y);
            };
        // global_var_grid_width = (2 * rows + 1) * height;
        // alert(global_var_grid_width);

        for (row = 0; row < rows; row++) {
            for (column = 0; column < columns; column++) {
                center = { x: Math.round((1 + 1.5 * column) * radius), y: Math.round(height * (1 + row * 2 + (column % 2))) };
                createSVG('polygon').attr({
                    points: [
                        toPoint(-1 * radius / 2, -1 * height),
                        toPoint(radius / 2, -1 * height),
                        toPoint(radius, 0),
                        toPoint(radius / 2, height),
                        toPoint(-1 * radius / 2, height),
                        toPoint(-1 * radius, 0)
                    ].join(' '),
                    'class': cssClass,
                    tabindex: 1
                })
                    .appendTo(svgParent).data({ center: center, row: row, column: column }).on('click', hexClick).attr({ 'hex-row': row, 'hex-column': column });
            }
        }
    });
};


var clickedSearch = function () {
    playedSkip = false;
    $("#message-content").fadeOut(300);
    $("#message-content2").fadeOut(300);
    $("#message-search").fadeOut(300);
    $("#message-skip").fadeOut(300);
    $("#message").fadeOut(300);
    $("#container").css('filter', 'blur(0px)');
    $("#cover").hide();
}

//function being triggered when player plays with square option
var clickedSkip = function (pos) {
    playedSkip = true;

    clearTimeout(timer);
    $("#cover").css('z-index', '2');
    $("#message-content2").fadeOut(200);
    $("#message").fadeOut(200);
    $("#message-search").fadeOut(600);
    $("#message-skip").fadeOut(600);
    $("#message-content").text("Waiting for other players to make their choice...");
    $("#message-content-background").css('background', 'rgba(34, 93, 160, 0.7)');
    $("#message-content").css('top', '28%');
    // $("#message-content").css('font', '7em bold');
    $("#message-content-background").show();



    socket.emit('play', { subjectID: subID, x: pos, y: pos ,currentCost:currentRoundCost});
}


var rebuild = function (Hl, Hw, Nsquares) {
    var
        radius = 20,
        columns = Hw,
        rows = Hl,
        cssClass = 'hexfield';


    $('#container').empty().hexGridWidget(radius, columns, rows, cssClass);
    $('#squares').empty();
    $('#container .hexfield').click(function () {
        clearTimeout(timer);
        if (!this.classList.contains('clicked')) {
            this.classList.add('clicked');
            $("#cover").show();
            $("#container").css('filter', 'blur(2px)');
            $("#message-content").css('top', '28%');
            // $("#message-content").css('font', '7em bold');
            $("#message-content").text("Waiting for other players to make their choice...");
            $("#message-content").fadeIn(500);
            $("#message-content-background").fadeIn(500);
            console.log('[' + this.getAttribute('hex-row') + ',' + this.getAttribute('hex-column') + ']');
            socket.emit('play', { subjectID: subID, x: this.getAttribute('hex-row'), y: this.getAttribute('hex-column'),currentCost:currentRoundCost });
        }

    });


};



///////////////////////////////////////////////////////////////////



$("#sign-in").on('click', function () {
    //sends the form for joining, shows error if something is not valid.
    if (validateForm()) {
        subID = $("#subjectID").val();

        socket.emit('new subject', {
            subjectID: $("#subjectID").val(),
            age: $("#age").val(),
            gender: $("#genderM").is(':checked') ? 'M' : 'F',
            //inputLine is a dict for the out put csv file in the future
            inputLine: {
                Hl: $("#Hl").val(), Hw: $("#Hw").val(), P: $("#chRate").val(), C: $("#cost").val(), V: $("#payoff").val(), Condition: $("#mode").val(), NG: $("#Ngames").val(), NR: $("#Nrounds").val(), Nplayers: $("#Nplayers").val(), ID: $("#subjectID").val(), age: $("#age").val(), gender: $("#genderM").is(':checked') ? 1 : 0,
                G: 0, R: 0, NOpenDiscoveries1: 0, NOpenDiscoveries2: 0, NOpenDiscoveries3: 0, IDOpenDiscoveries1: [],
                IDOpenDiscoveries2: [], IDOpenDiscoveries3: [],
                IDSelfOpenDiscoveries: [], Hive: 0, Hexagon: 0,
                Payoff: 0, reward: 0, extra: 0, order: 0, Yellow: 0, Red: 0,costs:[],lastRoundCost:0
            }
        }, function (valid) {
            if (valid) {
                $("#form").fadeOut(1000, () => {
                    $.get("/settings", function (data) {
                        Hl = data.Hl;
                        Hw = data.Hw;
                        Nsquares = data.Nsquares;
                        timeout = data.timeoutTime * 1000;
                        messageTime = data.messageTime * 1000;
                        settings = data;
                        //////////////////////////////////////////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////////////////////////////////////////  
                        
                        isSingleton=valid.singleton;
                        setInstructions(valid.singleton);
                        let otherPlayersNum = data.Nplayers - 1;
                        let txtStr1 = "<br/>במשחק זה תשחקו במקביל לעוד " + otherPlayersNum.toString() + " שחקנים,ומטרתכם להשיג את מספר הנקודות הרב ביותר.";
                        let txtStr2="במשחק זה מטרתכם להשיג את מספר הנקודות הרב ביותר.";

                        let txtStr3="שטח החיפוש משותף לכולם, ולכן פעולות השחקנים האחרים משפיעות על הרווחים שלכם.";

                        if (!isSingleton && otherPlayersNum>0) {
                            $("#paralelMessage").html(txtStr1);
                            $("#paralelMessage2").html("<p>"+txtStr3+ "</p>");
                        }else{
                            $("#paralelMessage").html(txtStr2);
                        }
                        $("#instruction-general").fadeIn(1000);


                        ////////////////////////////////////////////////////



                        // socket.emit('ready');
                        // $("#instructions2").fadeOut(500);
                        // ready = true;
                        // rebuild(Hl, Hw, Nsquares);
                        // document.body.style.zoom = "75%"

                        ///////////////////////////////////////////////////
                        // changeGameState('searchSkip');
                        // activateClient();
                    });

                });
            }
            else {
                showError('This ID already exists!')
            }
        })

    }
    else {
        showError('Make sure your form is valid!');
    }
});

$("#ready-to-start").on('click', () => {
    // sends signal that this player is ready to start
    socket.emit('ready');
    $("#instructions2").fadeOut(500);
    ready = true;
    rebuild(Hl, Hw, Nsquares);
    document.body.style.zoom = "75%"
});

$("#continue-general").on('click', () => {
    $("#instruction-general").fadeOut(1000, () => {
        $("#instructions").fadeIn(1000);
    });
    window.scrollTo({ top: 0, behavior: 'smooth' });
});

$("#continue").on('click', () => {
    // moving from the 1st instructions page to exam.
    $("#instructions").fadeOut(1000, () => {
        $("#instruction-NG").html(settings.Ngames);
        $("#instruction-NR").html(settings.Nrounds);
        $("#instruction-exchange").html('נקודה 1 = ' + (1 / settings.exchangeRate).toFixed(2) + settings.currency);
        $("#instruction-showup").html(settings.showUp + ' ' + settings.currency);
        $("#instruction-showup2").html(settings.showUp + ' ' + settings.currency);
        $("#exam").fadeIn(1000);
    });
    window.scrollTo({ top: 0, behavior: 'smooth' });
});

$("#exam-answer").on('click', () => {
    // moving from the exam page to the 2nd instructions page.
    if (checkAnswers()) {
        $("#exam").fadeOut(1000, () => {
            $("#instructions2").fadeIn(1000);
        });
    }
});

var startNextGame=function(){
    $("#betweenGames").fadeOut(500, () => {
        $("#game").fadeIn(500);
        $("#cover").show();
        showGameSearchSkipMessage();
        $("#message-search").fadeIn(200);
        setTimerOn();
        newGame = false;
    });
}

$("#start-next").on('click', () => {
    // starts next game (timer is on)
    $("#betweenGames").fadeOut(500, () => {
        $("#game").fadeIn(500);
        $("#cover").show();
        showGameSearchSkipMessage();
        $("#message-search").fadeIn(200);
        setTimerOn();
        newGame = false;
    });
});

socket.on('start', function (data) {
    // starting the first game
    start = true;
    gameSettings = data;
    generateCostForNewRound();



    if (ready) {
        if ($("#waiting").is(":visible")) {
            $("#waiting").fadeOut(500, () => {
                $("#game").fadeIn(500);

                $("#cover").show();
                $("#container").css('filter', 'blur(2px)');
                $("#squareButtons").css('filter', 'blur(2px)');
                $("#message").css('background', 'rgba(34, 93, 160, 0.7)');
                showGameSearchSkipMessage();

            });
        }
        else {
            $("#game").delay(400).fadeIn(500);

            $("#cover").show();
            $("#container").css('filter', 'blur(2px)');
            $("#squareButtons").css('filter', 'blur(2px)');
            $("#message").css('background', 'rgba(34, 93, 160, 0.7)');
            showGameSearchSkipMessage();


        }
        setTimerOn();
    }
});


socket.on('next-round', function (data) {
    //coloring the grid


    for (var i in data.answer.hexs) {
        tuple = i.split(',');
        var hexRow = tuple[0];
        var hexCol = tuple[1];
        if (hexRow != -1 && hexRow != -2) {
            var hex = $("polygon[hex-row=" + hexRow + "][hex-column=" + hexCol + "]")[0];
            hex.classList.add('clicked');

            if (data.answer.hexs[i]) {
                if (data.answer.hexs[i] == 'others') {
                    hex.classList.add('foundCorrect');

                }
                else {
                    hex.classList.add('clickedCorrect');

                }
            }
            else {
                hex.classList.add('clickedWrong');

            }
        }
    }

    for (var root in data.answer.roots) {

        var neighs = data.answer.roots[root].neighbors;

        for (var neigh in neighs) {
            if (neighs[neigh] == null) {
                continue;
            }
            // tuple = neigh.split(',');
            var hexRow = neighs[neigh][0];
            var hexCol = neighs[neigh][1];

            hex = $("polygon[hex-row=" + hexRow + "][hex-column=" + hexCol + "]")[0];

            if (!data.answer.roots[root].mine && !hex.classList.contains('blackBorder')) {
                hex.classList.add('redBorder');
            } else if (data.answer.roots[root].mine) {
                if (hex.classList.contains('redBorder')) {
                    hex.classList.remove('redBorder');
                }
                hex.classList.add('blackBorder');
            }
        }
    }

    for (var close in data.answer.toClose) {

        var neighs = data.answer.toClose[close];

        for (var neigh in neighs) {
            if (neighs[neigh] == null) {
                continue;
            }
            // tuple = neigh.split(',');
            var hexRow = neighs[neigh][0];
            var hexCol = neighs[neigh][1];

            hex = $("polygon[hex-row=" + hexRow + "][hex-column=" + hexCol + "]")[0];

            if (hex.classList.contains('blackBorder')) {
                hex.classList.remove('blackBorder');
            }

            if (hex.classList.contains('redBorder')) {
                hex.classList.remove('redBorder');
            }

        }
    }



    // showing the payoff for this round, and starting a new round (sets time on)

    removeGameWaitingForOtherMessage();
    setTimeout(() => {
        let thecost = -currentRoundCost;
        let thepayoff = Number(data.answer.payoff) + thecost;

        if (playedSkip) {
            showPayoffMessageForSkipped();
        } else {
            setOldPropsAfterSkipPayedoff();
            $("#revenue").html(data.answer.payoff);
            if (data.answer.payoff == 0) {
                $("#revenue").css('color', 'white');
            } else {
                $("#revenue").css('color', 'yellow');

            }

            $("#costRes").html(thecost);
            if (thecost == 0) {
                $("#costRes").css('color', 'white');
            } else {
                $("#costRes").css('color', 'red');
            }

            $("#thePayoff").html(thepayoff);
            if (thepayoff == 0) {
                $("#thePayoff").css('color', 'white');
            } else if (thepayoff < 0) {
                $("#thePayoff").css('color', 'red');
            } else {
                $("#thePayoff").css('color', 'yellow');
            }
        }


        showGamePayoffMessage();
        generateCostForNewRound();
        setTimeout(() => {
            removeGamePayoffMessage();
            if (!(gameSettings.Nrounds + 1 == data.round)) {
                setTimeout(() => {
                    gameSettings.cost = data.cost;//update new round cost
                    showGameSearchSkipMessage();
                    $("#message-search").fadeIn(100);
                    $("#cover").css('z-index', '2');
                }, 500);
            // changeGameState('searchSkip');

            }


        }, gameSettings.messageTime * 1000);



    }, 500);


    s = 'game: ' + data.game + ', round: ' + data.round;
    $("#test").html(s);
});


socket.on('disconnect', function (data) {
    clearTimeout(timer);
    socket.disconnect();
    $("#cover").show();
    $("#container").css('filter', 'blur(2px)');
    $("#message-content-background").css('background', 'rgb(226, 0, 0)');
    $("#message-content").css('top', '28%');
    // $("#message-content").css('font', '7em bold');
    $("#message-content").html("Game stopped due to too many timeouts");
    $("#message-content2").fadeOut(300);
    $("#message-content-background").show();
    $("#message-content").show();

});


socket.on('next-game', function (data) {

    newGame = true;
    generateCostForNewRound();
    $("#game").fadeOut(500, () => {
        $("#NG").html(data.game);
        $("#NR").html(data.rounds);
        $("#betweenGames").fadeIn(500);
        rebuild(Hl, Hw, Nsquares);

    });
    $("#cover").hide()
    s = 'game: ' + data.game + ', round: ' + data.round;
    $("#test").html(s);

});

socket.on('finished-all', function (data) {
    // final message upon finishing all games - shows the player its reward
    newGame = true;

    removeGameSearchSkipMessage();
    $("#message-content").fadeOut(200);
    $("#message-content-background").fadeOut(200);
    $("#cover").css('opacity', '0.0');

    clearTimeout(timer);
    $("#game").delay(messageTime + 500).fadeOut(500, () => {
        var step = data.step + 1;
        $("#chosenGame").html(Math.ceil(step / settings.Nrounds));
        $("#chosenRound").html((step - 1) % settings.Nrounds + 1);
        $("#chosenValue").html(data.value);
        $("#reward").html(Math.max(0, settings.showUp + Math.round((data.value / settings.exchangeRate))) + settings.currency);
        $("#showUp-explain").html('Show Up reward is ' + settings.showUp + settings.currency);
        $("#rate").html('1 point = ' + (1 / settings.exchangeRate) + settings.currency);
        $("#thankyou").fadeIn(500);
    });
    gameIsOn=false;
    s = 'DONE';
    $("#test").html(s);
});

socket.on('waiting', function (data) {
    // waiting for players to join the game
    if (!start) {
        if (!$("#waiting").is(":visible") && ready) {
            $("#waiting").delay(400).fadeIn(500);
        }
        if ($("#nplayers").html() != data) {
            $("#nplayers").fadeOut(200, () => {
                // updates the number of remaining players needed to begin
                $("#nplayers").html(data);
                $("#nplayers").fadeIn(200);
            });
        }
    }
});

socket.on('disconnect', function (data) {
    /* when the sockets disconnect (due to connection problems or forced by the server because of timeous)
        shows an error information */
    clearTimeout(timer);
    socket.disconnect();
    $("#cover").show();
    $("#container").css('filter', 'blur(2px)');
    $("#message-content-back").css('background', 'rgb(226, 0, 0)');
    $("#message-content").html("Game stopped due to too many timeouts");
    $("#message-content-background").show();
    $("#message-content").show();
});

socket.on('restart', function () {
    location.reload();
})

function showPayoffMessageForSkipped() {
    $("#revenue-title").css("display", "none");
    $("#revenue").css("display", "none");
    $("#costRes").css("display", "none");
    $("#equal").css("display", "none");
    $("#thePayoff-title").css("display", "none");
    $("#thePayoff").css("display", "none");
    $("#cost-title").css("font-size", "10vw");

    $("#cost-title").html("0");
}

function setOldPropsAfterSkipPayedoff() {
    $("#revenue-title").css("display", "initial");
    $("#revenue").css("display", "initial");
    $("#costRes").css("display", "initial");
    $("#equal").css("display", "initial");
    $("#thePayoff-title").css("display", "initial");
    $("#thePayoff").css("display", "initial");
    $("#cost-title").css("font-size", "5vw");
    $("#cost-title").html("cost");
}

function showGameSearchSkipMessage() {
    $("#message-content").css('top', '28%');
    // $("#message-content").css('font', '7em bold');
    $("#message-content").html('Searching cost now is:');
    // $("#message-content2").html(-gameSettings.cost);
    $("#message-content2").html(-currentRoundCost);


    $("#message").fadeIn(200);
    $("message-search").fadeIn(200);
    $("#message-skip").fadeIn(200);

    $("#message-content").fadeIn(200);
    $("#message-content2").fadeIn(200);

}

function removeGameSearchSkipMessage() {
    $("#message-content").fadeOut(200);
    $("#message-content2").fadeOut(200);
    $("#message-content-background").fadeOut(200);
    $("#message").fadeOut(200);
    $("#message-search").fadeOut(200);
    $("#message-skip").fadeOut(200);
}

function showGameWaitingForOtherMessage() {
    $("#message-content").html("Waiting for other players to make their choice...");
    $("#message-content").css('top', '28%');
    // $("#message-content").css('font', '7em bold');
    $("#message-content").fadeIn(200);
    $("#message-content-background").fadeIn(200);
}

function removeGameWaitingForOtherMessage() {
    $("#message-content").fadeOut(200);
    $("#message-content-background").fadeOut(200);
}

function showGamePayoffMessage() {
    $("#message-earning").fadeIn(200);
}

function removeGamePayoffMessage() {
    $("#message-earning").fadeOut(200);
}

function validateForm() {
    if ($("#subjectID").val().length < 1 || !$.isNumeric($("#subjectID").val()) || $("#age").val().length < 1 || !$.isNumeric($("#age").val()) || (!$("#genderM").is(':checked') && !$("#genderF").is(':checked'))) {
        return false;
    }
    else {
        return true;
    }
}

function showError(error) {
    $("#form-error").html(error);
    $("#form-error").show();
}

function setTimerOn() {
    timer = setTimeout(() => {
        clickedSkip(-2);
    }, timeout);
}

function setInstructions(mode) {
    // sets the right paragraph of instructions visible according to the game mode.
    let modeName = mode || settings.gameMode;
    switch (modeName) {
        case "Patent":
            $("#patent-instructions").attr("hidden", false);
            setExam(examObj.patent);
            break;
        case "No Patent":
            $("#noPatnet-instructions").attr("hidden", false);
            setExam(examObj.noPatent);
            break;
        case "Trade Secrets":
            $("#trade-instructions").attr("hidden", false);
            setExam(examObj.trade);
            break;
        default:
            $("#singleton-instructions").attr("hidden", false);
            setExam(examObj.singleton);
    }
}


function setExam(obj) {
    $("#exam").find("h4").each(function (index) {
        $(this).text(obj.questions[index]);
    }); //sets the questions
    $("#exam").find("span").each(function (index) {
        if (index > 15)
            return;
        $(this).html(obj.answers[index]);
    }); //sets the questions
}

function checkAnswers() {
    var flag = true;
    $("#qError").hide();
    if (!$('#answer1').is(':checked')) {
        flag = false;
        $("#qNum").text("1");
        $("#qError").show();
    }
    if (!$('#answer2').is(':checked')) {
        flag = false;
        $("#qNum").text("2");
        $("#qError").show();
    }
    if (!$('#answer3').is(':checked')) {
        flag = false;
        $("#qNum").text("3");
        $("#qError").show();
    }
    if (!$('#answer4').is(':checked')) {
        flag = false;
        $("#qNum").text("4");
        $("#qError").show();
    }

    return flag;
}