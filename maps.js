
const mapping = require('./mapping');
var fs = require('fs');

var mapsOutputString;

var gameSettings = {
  Ngames: 1,
  Nrounds: 3,
  Nplayers: 1,
  Nsquares: 10,
  NClicked: 0,
  currentGame: 1,
  currentRound: 1,
  Hl: 22,
  Hw: 46,
  CHrate: 0.1,
  cost: 1,
  payoff: 2,
  currency: '&#8362;',
  exchangeRate: 0.5,
  showUp: 20,
  timeoutTime: 500,
  timeoutPenalty: 10,
  timeoutKick: 3,
  messageTime: 2.5,
  gameMode: 'Singleton',
};
var i = 0;

var initalMapsString = [];

for (k = 0; k < gameSettings.Hl; k++) {
  for (t = 0; t < gameSettings.Hw; t++) {
    tuple = "(" + k + "-" + t + ")";
    initalMapsString.push(tuple);
  }
}

mapsOutputString = initalMapsString + "\n";

for (i = 0; i < 6000; i++) {

  var map = mapping.mapping(gameSettings);

  var currentMapString = [];

  for (k = 0; k < gameSettings.Hl; k++) {
    for (t = 0; t < gameSettings.Hw; t++) {
      currentMapString.push(map[k][t]);
    }
  }

  mapsOutputString += currentMapString + "\n";

  printTofile(i);

}


function printTofile(i) {
  if (i == 5999) {


    var path = 'public/data/';

    console.log(path);
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }

    name = path + '/Maps.csv';
    var ws = fs.createWriteStream(name);
    ws.write(mapsOutputString);


  }
}
